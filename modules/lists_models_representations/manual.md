# Работа со списками

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Разберемся, как устроены списки и что такое архитектура __Model-View__.

Рассмотрим обычный список, с которым имеем дело в телефонной книге контактов. У нас есть визуальное отображение информации. Каждый контакт отображается как некий виджет с иконкой, текстом, изображением и т.д. Этой книге соответствует некая модель данных. На рисунке список `ListElement`, содержащий имя, фамилию, номер телефона.

Каждый элемент, визуально содержащий список или другое множество элементов в QML, должен обладать моделью данных и делегатом. Делегат — это специальный элемент, который отвечает за отображение элемента модели данных.

Для приведенного примера можно использовать `ListModel`, массив элементов `ListElement`. Пример описания модели данных:

![](../../resources/117.png)

`ListView` — это специальный визуальный элемент, предоставляющий возможности по отображению набора элементов. Для функционирования `ListView` разрабатывают три компонента:

*	основной файл, содержащий `ListView`;
*	модель, содержащую наборы данных;
*	делегат.

Модель может быть помещена в отдельный файл или в тот же, где находится `ListView`.

![](../../resources/118.png)

Делегат является наследником `Item` и отвечает за корректное отображение элемента списка.

Создадим простой делегат в отдельном qml файле.

```qml
Item {
    id:deleg
    property string name: "?"
    property string surname: "?"
    property string phoneNumber: "?"
    
    Text{ text:name }
    Text{ text:surname}
    Text{ text:phoneNumber }
}
```

Заготовка для главного файла. 

```qml
ListModel{    }
Component{    }
ListView {    }
```

Для ListModel создаем несколько записей, меняя атрибуты. 

```qml
ListModel{
    id:my_model
    ListElement{ name:"Ivan"; surname:"Petrov";phone:"11-11-11"}
    ListElement{ name:"Petr"; surname:"Ivanov";phone:"22-22-22"}
    ListElement{ name:"Pavel"; surname:"Sidorov";phone:"33-33-33"}
}
```

Создаем `Component` для делегата. Делегат будет создан далее, пока есть только его тип. Описываем экземпляр `Component`, для начала отобразив только имена:

```qml 
Component{
    id:my_delegate
    MyDelegate{
        name:model.name
        /*
           surname:model.surname
           phoneNumber: model.phone
           */
        width:parent.width
        height:40
    }
}
```

Связываем воедино делегат и модель.

```qml
ListView{
    id:my_list
    anchors.fill:parent
    model:my_model
    delegate:my_delegate
}
```

Код в едином листинге:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

ListModel{
    id:my_model
    ListElement{ name:"Ivan"; surname:"Petrov";phone:"11-11-11"}
    ListElement{ name:"Petr"; surname:"Ivanov";phone:"22-22-22"}
    ListElement{ name:"Pavel"; surname:"Sidorov";phone:"33-33-33"}
}
Component{
    id:my_delegate
    MyDelegate{
        name:model.name
        /*
           surname:model.surname
           phoneNumber: model.phone
           */
        width:parent.width
        height:40
    }
}
ListView{
    id:my_list
    anchors.fill:parent
    model:my_model
    delegate:my_delegate
}
```

Получаем в результате некоторый прокручиваемый список. 

Чуть изменим визуальный вид отображения списка в файле MyDelegate. 

В делегате используем компоновщик `Row`, чтобы выводились имена, фамилии и номера телефонов.

```qml
Item {
    id:deleg
    property string name: "?"
    property string surname: "?"
    property string phoneNumber: "?"
    
    Row{
        id:row
        anchors.left:parent.left
        anchors.right: parent.right
        height:parent.height
        spacing:6
        
        Text{ text:name }
        Text{ text:surname}
        Text{ text:phoneNumber}
    }
}
```

![](../../resources/82_1.png)

Добавим границу, градиентную заливку, отступы.

```qml
Item {
    id:deleg
    property string name: "?"
    property string surname: "?"
    property string phoneNumber: "?"
    
    Rectangle{
        id:rect
        border.color:"darkgrey"
        radius:5
        anchors.fill:parent
        gradient:Gradient{
            GradientStop{position:0;color:"lightgray"}
            GradientStop{position:1;color:"white"}
        }
    }
    Row{
        id:row
        anchors.left:parent.left
        anchors.right: parent.right
        height:parent.height
        anchors.margins: 16
        spacing:6
        
        Text{text:name;anchors.verticalCenter: row.verticalCenter}
        
        Text{ text:surname; anchors.verticalCenter: row.verticalCenter}
        Text{ text:phoneNumber; anchors.verticalCenter: row.verticalCenter}
    }
}
```

![](../../resources/83_1.png)

Далее можно по своему усмотрению добавлять иконки, менять размер шрифта, гарнитуру, цвет, градиент. Можно изменить тип компоновщика с `Row` на `Column` или `Grid` и получить другое отображение списка. 

Подробное описание свойств типа `ListView` — в официальной [документации](https://doc.qt.io/qt-5.6/qml-qtquick-listview.html).
