# Возможные ответы на задания по теме «Работа со списками»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание 1

![](../../resources/110.png)

main.qml

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5

Window {
   width: 360
   height: 640
   visible: true
   title: qsTr("Task_for_ListView_Model")
   property int defMargin: 8
   Page{
       id:page
       anchors.fill:parent

       footer: PageFooter {
            onNewMessage: {
                //Append new message
                var newMsg = {};
                newMsg.name = msg;
                newMsg.time = Qt.formatTime(new Date(), "hh:mm");
                my_model.append(newMsg);
            }
        }

       Rectangle{
           id:fon
           anchors.fill:parent
           gradient:Gradient{
                GradientStop{position:0;color:"#d3d78d"}
                GradientStop{position:1;color:"#99bb8c"}
           }

            ListModel{
                id:my_model
                ListElement{ name:"text1 text1 text1"; time:"12:00"}
                ListElement{ name:"text2text2text2"; time:"12:20"}
                ListElement{ name:"text3 text3 text3"; time:"12:30"}
            }
            Component{
                id:my_delegate
                MyDelegate{
                    name:model.name
                    time:model.time
                    width:parent.width
                    height:40
                }
            }
            ListView{
                id:my_list
                anchors.fill:parent
                anchors.leftMargin: 3*defMargin
                anchors.rightMargin: 3*defMargin
                anchors.topMargin: 3*defMargin
                model:my_model
                delegate:my_delegate
                ScrollBar.vertical: ScrollBar{}
                spacing: 10
            }
        }
    }
}
```

MyDelegate.qml

```qml
import QtQuick 2.0

Item {
   id:deleg
   property string name: "?"
   property string time: "?"

   Rectangle{
        id:rect
        border.color:"darkgrey"
        radius:5
        anchors.fill:parent
        gradient: Gradient {
            GradientStop{position:0;color:"lightgray"}
            GradientStop{position:1;color:"white"}
        }
        Text{
            text:name
            anchors.fill: rect
            anchors.margins:5
        }
        Text{
            text:time;
            anchors.right:rect.right
            anchors.bottom: rect.bottom
        }
   }
}
```

PageFooter.gml

```qml
import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle{
   id:root
   height:60

   gradient:Gradient{
       GradientStop{position:0;color:"lightgray"}
       GradientStop{position:1;color:"white"}
   }
   signal newMessage(string msg)

   TextField {
        id: edtText
        selectByMouse: true
        anchors.fill:root
        placeholderText: "Write a message..."
        font.pointSize: 10
        color: "black"
    }
    Button {
        id: btnAddItem
        height:root.height
        anchors.right:parent.right
            text: "Send"
            onClicked: {
                 newMessage(edtText.text);
                 edtText.clear();
            }
    }
}
```
