# Вопросы по теме «Работа со списками»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Что такое делегат?
2.	Каково назначение `ListView`?
3.	Какие компоненты необходимы для функционирования `ListView`?
4.	Какую информацию содержит модель данных?
