# Переход по страницам приложения

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Для навигации между страницами приложения используют контейнер визуальных компонентов `StackView`. [Стек](https://doc.qt.io/qt-5/qml-qtquick-controls2-stackview.html) — это набор элементов, организованный определенным образом. Если представить мобильное приложение как набор экранов и сложить их в стопку, то видимым будет только верхний экран, т.е. последний элемент, который был в стек добавлен.

Основные операции в стеке — добавление элемента и извлечение из стека. Операция добавления называется push. Операция извлечения — pop. После добавления новой страницы она скрывает предыдущую и отображается на экране. Глубиной стека называют количество страниц в нем. При добавлении страниц глубина увеличивается, при извлечении — уменьшается. Глубина стека не может быть равна нулю. Всегда есть некоторая страница, которая должна быть в стеке. Для этого есть специальное свойство `initialItem`. 

Есть дополнительный метод `replace`, позволяющий менять местами страницы в стеке, даже если страница не на вершине стека.

Тип `StackView` находится в модуле Controls. `StackView` необходимо добавить и растянуть на весь экран посредством якоря `anchor.fill:parent`. 

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")
    
    property int defMargin:10
    name: value
    
    StackView{
        id:stack_view
        anchors.fill: parent
    }
}
```

В качестве элементов `StackView` выступают компоненты. Компоненты лучше создавать в отдельном файле. 

В качестве корневого элемента будет выступать тип `Page`. Переносим его в отдельный файл. 

Внутри страницы объявим `alias`-свойства для фона страниц и названий кнопок (чтобы можно было их менять из main.qml).

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Page{
    id:root
    property alias backgroundColor:back_fon.color
    property alias buttonText:batton_nav.text
    signal buttonClicked();
    
    background: Rectangle{
        id:back_fon
    }
    Button {
        id:batton_nav
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defMargin // look into main.qml
        onClicked: {
            root.buttonClicked()
        }
    }
}
```

Добавляем пару страниц в стек. Назначаем первую страницу в `initialItem`. А видимость остальных страниц пока скрываем (иначе будут отображаться все). Через `alias` задаем цвет фона страниц и названия кнопок.

При  использовании метода `pop()` в скобках указываем название извлекаемой страницы. Если указать название стартовой страницы, то из стека будут извлечены все страницы, находящиеся выше page1.	

Итоговый код примера с переключением между двумя страницами:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test")
    
    property int defMargin:10
    
    StackView{
        id:stack_view
        anchors.fill: parent
        initialItem: page1
    }
    My_Page { id:page1
        backgroundColor: "red"
        buttonText: "To_Green"
        onButtonClicked: {
            stack_view.push(page2)
        }
    }
    My_Page { id:page2
        visible: false
        backgroundColor: "green"
        buttonText: "To_Red"
        onButtonClicked: {
            stack_view.pop(page1)
        }
    }
}
```

Код страницы:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Page{
    id:root
    property alias backgroundColor:back_fon.color
    property alias buttonText:batton_nav.text
    signal buttonClicked();
    
    background: Rectangle{
        id:back_fon
    }
    Button {
        id:batton_nav
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defMargin // look into main.qml
        onClicked: {
            root.buttonClicked()
        }
    }
}
```

![](../../resources/40.png)

Доработаем предыдущий пример. Добавим в `Page` обработку сигнала возврата с предыдущей страницы.

В main.qml допишем функцию `pop` для нашего стека. Определенная в головном файле, она будет доступна и в странице компонента `My_Page`. Для кнопки `button_nav` добавляем возможность быть видимой только в случае, если она содержит некоторый текст.

```qml
visible:text.length>0
```

В My_Page.qml добавим `header`, общий для всех страниц, и будем добавлять в него некоторые кнопки, например, кнопку возврата на предыдущую страницу. Поскольку эта кнопка не должна быть видимой на первой странице приложения, т.к. с первой страницы выход невозможен, настраиваем ее видимость в зависимости от глубины стека (если глубина больше единицы, текст на кнопке будет видим). Также добавим в `header` текст о текущей странице. Текст можно регулировать в mail.qml при добавлении страницы в стек, поэтому в Page.qml это свойство опишем через `alias`. 

```qml
property alias headerText:header_page_text.text
```

Поскольку в `header` нужно разместить несколько элементов, обернем их контейнером `RowLayout`.

```qml
header:ToolBar{
    id:page_header
    height:40
    RowLayout{
        ToolButton{
            id:back_btn
            Text{
                text: "<-"
                font.pixelSize: 24
                visible:stack_view.depth>1
                anchors.verticalCenter: parent.verticalCenter}
            onClicked:  {popPage()}
        }
        Text{
            id:header_page_text
            anchors.centerIn: page_header
        }
    }
}
```

Добавим по центру экрана название активной страницы. Название считывается из свойства `title` текущей страницы стека (верхней страницы).

```qml
Text{
       anchors.centerIn: parent
       font.pointSize: 18
       text: stack_view.currentItem.title
   }
```

Свойство `title` определяем в main.qml при объявлении страниц стека.

Итоговый текст main.qml:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Window {
    id:win
    width: 360
    height: 640
    visible: true
    title: qsTr("StackView_test_1")
    
    property int defMargin:10
    
    
    
    function popPage() {
        stack_view.pop()
    }
    
    StackView{
        id:stack_view
        anchors.fill: parent
        initialItem: page1
    }
    My_Page {
        id:page1
        title:"First Page"
        buttonText: "Next"
        headerText:"It's a first page"
        onButtonClicked: {
            stack_view.push(page2)
        }
    }
    My_Page {
        id:page2
        title:"Second Page"
        visible: false
        buttonText: "Back"
        headerText:"It's a second page"
        onButtonClicked: {
            stack_view.pop(page1)
        }
    }
    
    Text{
        anchors.centerIn: parent
        font.pointSize: 18
        text: "Content of " + stack_view.currentItem.title
    }
    
}
```

Итоговый код `My_Page`:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.2

Page{
   id:root
   anchors.fill:win
   property alias backgroundColor:back_fon.color
   property alias buttonText:batton_nav.text
   property alias headerText:header_page_text.text
   signal buttonClicked();

   background: Rectangle{
       id:back_fon
       }

   header:ToolBar{
       id:page_header
       height:40
       RowLayout{
       ToolButton{
           id:back_btn
           Text{
           text: "<-"
           font.pixelSize: 24
           visible:stack_view.depth>1
           anchors.verticalCenter: parent.verticalCenter}
           onClicked:  {popPage()}
                 }
       Text{
           id:header_page_text
           anchors.centerIn: page_header
       }
       }
   }

   Button {
       id:batton_nav
       visible:text.length>0
       anchors.right: parent.right
       anchors.bottom: parent.bottom
       anchors.margins: defMargin // look into main.qml
       onClicked: {
           root.buttonClicked()
       }
   }
  Keys.onBackPressed: {
      popPage()
  }
}
```

![](../../resources/43.png)

## Жесты в интерфейсе

Существует несколько типов жестов, которые можно использовать в QML:

*	`tap` (касание) — это простейший жест, который срабатывает, когда пользователь коснулся экрана. Он может быть использован для запуска каких-либо действий или открытия других окон;
*	`swipe` (проведение) — это жест, который срабатывает, когда пользователь проводит пальцем по экрану в определенном направлении. Он может быть использован для прокрутки списков или перехода между страницами;
*	`pinch` (масштабирование) — это жест, который срабатывает, когда пользователь двумя пальцами масштабирует элементы на экране. Он может быть использован для масштабирования картинок или изменения масштаба элементов интерфейса;
*	`rotate` (поворот) — это жест, который срабатывает, когда пользователь двумя пальцами поворачивает элементы на экране. Он может быть использован для вращения картинок или изменения ориентации элементов интерфейса.

Для обработки жестов в QML можно использовать элементы `MouseArea`, `MultiPointTouchArea`, `SwipeView` и `PinchArea`. Например, `MouseArea` может быть использован для обработки жестов касания и проведения, а `PinchArea` может быть использован для обработки жестов масштабирования и поворота.

Пример использования элемента `MouseArea` для обработки жестов касания и проведения:

```qml
Rectangle {
    width: 200
    height: 200
    color: "gray"

    MouseArea {
        anchors.fill: parent
        onPressed: console.log("Касание началось")
        onReleased: console.log("Касание закончилось")
        onPositionChanged: console.log("Проведение", mouseX, mouseY)
    }
}
```

В приведенном примере создается прямоугольник, на который добавляется элемент `MouseArea`. При нажатии на прямоугольник выводится сообщение в консоль, а при проведении выводятся координаты мыши.

Конечная цель обработки жестов `Swipe` в QML Quick — позволить пользователю перелистывать содержимое экрана влево или вправо, например, для просмотра других страниц или прокрутки списка элементов. 

Для этого нужно использовать элемент `SwipeView`, позволяющий создавать горизонтальные или вертикальные списки, которые можно прокручивать жестами `Swipe`.

Вот пример использования элемента `SwipeView` для прокрутки горизонтального списка с помощью жестов `Swipe`:

```qml
SwipeView {
    width: 320
    height: 480

    // Создаем 3 страницы для прокрутки
    Page {
        Rectangle {
            color: "red"
            width: parent.width
            height: parent.height
            Text {
                text: "Первая страница"
                anchors.centerIn: parent
            }
        }
    }

    Page {
        Rectangle {
            color: "green"
            width: parent.width
            height: parent.height
            Text {
                text: "Вторая страница"
                anchors.centerIn: parent
            }
        }
    }

    Page {
        Rectangle {
            color: "blue"
            width: parent.width
            height: parent.height
            Text {
                text: "Третья страница"
                anchors.centerIn: parent
            }
        }
    }
}
```

Принцип работы элемента `SwipeView` в QML Quick можно описать следующим образом.

1.	Создание `SwipeView`. Элемент `SwipeView` может быть создан как любой другой элемент QML Quick. Например, можно создать его в качестве дочернего элемента другого элемента, чтобы использовать его в качестве контейнера.
2.	Добавление страниц. Далее необходимо добавить страницы в `SwipeView`. Это можно сделать, добавляя дочерние элементы типа `Page` в `SwipeView`. Каждая страница должна содержать элементы интерфейса, которые должны отображаться при прокручивании списка.
3.	Прокручивание SwipeView. Когда пользователь начинает свайпать по `SwipeView`, элемент определяет направление свайпа и плавно прокручивает список до следующей или предыдущей страницы. Это происходит с помощью анимации, которая плавно перемещает страницы влево или вправо (или вверх и вниз, если SwipeView настроен для вертикальной прокрутки).
4.	Обработка жестов. `SwipeView` автоматически обрабатывает жесты `Swipe` и прокручивает список соответствующим образом. Однако можно настроить дополнительные параметры, такие как чувствительность жеста, минимальное расстояние для срабатывания жеста, направление и т.д., чтобы изменить поведение `SwipeView` в зависимости от конкретных требований.
5.	Обработка сигналов. Элемент `SwipeView` генерирует сигналы, которые можно использовать для обработки событий, связанных с прокруткой. Например, можно использовать сигнал `onCurrentItemChanged`, чтобы определить, когда текущая страница в списке была изменена пользователем.

__Кастомизация SwipeView__

Элемент `SwipeView` в QML Quick позволяет кастомизировать внешний вид и поведение списка, чтобы он лучше соответствовал конкретным требованиям приложения. Некоторые из возможных способов кастомизации `SwipeView`:

*	изменение оформления — можно изменить оформление элементов S`wipeView`, чтобы оно больше соответствовало дизайну приложения. Например, `background`;
*	изменение поведения — можно изменить поведение `SwipeView`, чтобы оно больше соответствовало специфике приложения. Например, можно изменить скорость прокрутки, чувствительность жестов, размер страниц и другие параметры, чтобы `SwipeView` лучше отображал содержимое;
*	добавление анимации — можно добавить дополнительные анимации к `SwipeView`, чтобы сделать интерфейс более интерактивным и привлекательным для пользователей. Например, можно добавить анимацию перехода между страницами, чтобы делать прокрутку списка более плавной; 
*	использование вложенных элементов — можно использовать вложенные элементы в `SwipeView`, чтобы добавить дополнительные элементы интерфейса на каждой странице. Например, можно добавить кнопки, изображения, текстовые поля и другие элементы, чтобы улучшить функциональность `SwipeView`.

__Мышь для свайпа__

По умолчанию, жесты `Swipe` в QML Quick работают на сенсорных экранах, но в учебных задачах при начальной разработке интерфейса или при отсутствии сенсорного экрана можно использовать мышь.
Чтобы жест `Swipe` работал на компьютере с помощью мыши, необходимо включить поддержку мыши в параметрах элемента `SwipeView`, установив свойство interactive равным `SwipeView.ForceElasticity`. Например:

```qml
SwipeView {
    interactive: Swipe View.ForceElasticity
    // ...
}
```

Когда пользователь нажимает на элемент `SwipeView` и начинает перетаскивать его мышью, элемент `SwipeView` реагирует на это и обновляет свое содержимое, в соответствии с направлением свайпа. При отпускании кнопки мыши `SwipeView` переходит на следующую страницу или возвращает страницу на место, если перемещение было недостаточно длинным.

Это позволяет компенсировать отсутствие сенсорного экрана на устройстве. Таким образом, пользователь может перемещать содержимое экранов мышью, как если бы это был сенсорный экран.
