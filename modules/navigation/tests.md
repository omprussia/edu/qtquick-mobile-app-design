# Вопросы по теме «Переход по страницам приложения»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Что называют глубиной стека?
2.	Минимальное количество страниц в стеке?
3.	Свойство для установки начальной страницы стека?
4.	Команда добавления страницы в стек?
5.	Команда удаления страницы из стека?
