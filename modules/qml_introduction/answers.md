# Возможные ответы на задания по теме «Знакомство с QML»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Rectangle

![](../../resources/101.png)

```qml
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"red"
}
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"green"
    x:win.width/8
    y:win.width/8
}

Rectangle{
    width:win.width/4
    height: win.width/4
    color:"blue"
    x:win.width/4
    y:win.width/4
}
```

![](../../resources/102.png)

```qml
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"red"
    x:win.width/16
    y:win.width/16
}
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"green"
    x:win.width*6/16
    y:win.width/16
}
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"blue"
    x:win.width*11/16
    y:win.width/16
}
```

![](../../resources/103.png)

```qml
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"red"
    x:rect_green.x
    y:rect_green.y-1.1*win.width/4
}
Rectangle{
    id:rect_green
    width:win.width/4
    height: win.width/4
    color:"green"
    anchors.centerIn:parent
}
Rectangle{
    width:win.width/4
    height: win.width/4
    color:"blue"
    x:rect_green.x
    y:rect_green.y+1.1*win.width/4
}
```

## Text

![](../../resources/104.png)

```qml
Rectangle{
    width:win.width/1.3
    height: win.height/3
    color:"red"
    anchors.centerIn: parent

    Text {
        anchors.fill: parent
        text: "It's some text"
        color: "#00ff00"
        font.pixelSize: 28
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}
```
![](../../resources/105.png)

```qml
Rectangle{
    width:win.width/1.3
    height: win.height/3
    color:"red"
    anchors.centerIn: parent

    Text {
        anchors.fill: parent
        text: "It's some text"
        color: "#0000ff"
        font.pixelSize: 32
        verticalAlignment: Text.AlignBottom
        horizontalAlignment: Text.AlignRight
    }
}
```

## Gradient

![](../../resources/106.png)

```qml
Rectangle {
    color: "white"
    width: parent.width
    height: parent.height
    Text {
        id:task_text
        text: "Task 3"
        anchors.top:parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 5
    }
    gradient: Gradient {
        GradientStop{position:0 ;color: "medium sea green"}
        GradientStop{position:0.3 ;color: "pale green"}
        GradientStop{position:0.6 ;color: "light green"}
        GradientStop{position:1 ;color: "sea green"}
    }
}
```
