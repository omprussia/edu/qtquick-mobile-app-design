# Вопросы по теме «Знакомство с QML»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	В чем заключается декларативная парадигма программирования?
2.	Что предоставляет Qt Quick для разработки интерфейсов?
3.	Приведите примеры визуальных типов Qt Quick.
4.	Сколько частей содержит QML документ? 
5.	Как объявляется объект в QML документе?
6.	Какие свойства типа Item наследуют другие визуальные типы?
7.	Перечислите свойства типа Rectangle.
8.	Для чего используют тип Text?
9.	Какими способами может быть задано свойство объекта?
10.	Как установить привязку свойства между двумя элементами в QML, чтобы изменения одного элемента приводили к автоматическому обновлению другого элемента?
11.	Какие нотации поддерживает свойство color?
12.	Если в документе был задан градиент, а затем описано свойство color,  что отобразится в результате?
