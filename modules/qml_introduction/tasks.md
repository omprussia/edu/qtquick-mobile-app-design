# Задания по теме «Знакомство с QML»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Rectangle

Написать код, реализующий изменения свойств типа `Rectangle`: местоположение, цвет, размер, радиус скругления углов._

![](../../resources/51.png)

![](../../resources/52.png)

![](../../resources/53.png)

Выполнить задания, расположив прямоугольники в окне. Ширина окна может меняться (минимальные размеры: 360*640). Ширина и длина прямоугольника равны четверти ширины окна. Для построения использовать только привязанные свойства.

![](../../resources/90.png)

## Text

Отработать изменение свойств текста: размер, цвет, выравнивание.

![](../../resources/91.png)

## Градиент

Настроить градиент с переходом цветов для фона страницы. Можно использовать [таблицу цветов X11.](https://skobki.com/tablitsa-tsvetov-x11-v-pravilnom-poryadke/) 

![](../../resources/92.png)

## Image

Вставить изображение на страницу приложения. Настроить разные варианты заполнения fillMode. 

[Возможный вариант кода](./answers.md)
