# Вопросы по теме «Состояния. Переходы. Анимации»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Что описывает состояние?
2.	Каким свойством описывают набор состояний?
3.	Какая конструкция связывает состояние с некоторым логическим выражением?
4.	В чем измеряется длительность анимационных эффектов?
5.	Какие типы анимаций реализуют в дизайне интерфейсов?
6.	Для чего предназначены анимационные кривые?
7.	Какое свойство используют для установки анимационной кривой?
8.	Что задает `SequentialAnimation`?
9.	Если сгруппировать несколько анимаций с помощью `SequentialAnimation` и `SequentialAnimation` и `Parallel Animation`, то какой вариант будет выполняться дольше и почему?
