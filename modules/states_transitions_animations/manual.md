# Состояния. Переходы. Анимации

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Анимации используются для создания визуальных эффектов и применимы к любому визуальному элементу. Это основная задача использования анимации. Анимации могут применяться для чисел и свойств, их можно использовать для создания пауз и задержек. Существуют разные типы анимации:

*	`PropertyAnimation` – анимирует изменения значений свойств;
*	`NumberAnimation` – анимирует изменения в значениях типа qreal;
*	`ColorAnimation` – анимирует изменения значений цвета;
*	`RotationAnimation` – анимирует изменения значений вращения.

__Property Animation__ в QML позволяет создавать анимацию для изменения значения свойства элемента интерфейса. `Property Animation` определяет начальное и конечное значение свойства, продолжительность анимации, а также использование различных типов интерполяции между начальным и конечным значениями. 

Пример перемещения прямоугольника по горизонтали с помощью PropertyAnimation.

```qml
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    
    minimumWidth: 400
    minimumHeight: 450
    
    Rectangle{
        id:rect
        width:300; height:400
        anchors.centerIn:parent
        color:"lightgrey"
        
        Rectangle{
            id:rect_anim
            width:50; height:50
            border.color:"black"; border.width:1;
            color:"yellow"
            x:10; y:10
        }
        
        PropertyAnimation{
            id:anim
            target:rect_anim; property: "x";from:10; to: 240
            duration: 4000
            running:true
        }
    }
}
```

__Числовая анимация__ — частный случай анимации свойства `PropertyAnimation`. В этом случае пришлось бы подробнее описать объект, его свойство, начальное и конечное значения свойства, длительность и условие запуска `running`. В случае `running=true` анимация запускается сразу после загрузки свойств объекта. Если же `running=false`, то придется настроить обработчик, который в нужный момент времени эту анимацию запустит.
Пример использования простейшей числовой анимации.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    
    minimumWidth: 400
    minimumHeight: 450
    
    Rectangle{
        id:rect
        width:300; height:400
        anchors.centerIn:parent
        color:"lightgrey"
        
        Rectangle{
            id:rect_anim
            width:50; height:50
            border.color:"black"; border.width:1;
            color:"yellow"
            x:10; y:10
            
            NumberAnimation on x { from:10; to: 240; duration: 4000}
        }
    }
}
```

В приведенном примере анимация применяется к значению свойства элемента (x координата) с помощью ключевого слова `on`. Значение координаты меняется от начального значения x=10 до конечного значения x=240, длительность перехода 4 секунды (4000 миллисекунд). Прямоугольник перемещается в горизонтальном направлении слева направо.

Аналогично можно анимировать изменение координаты `y`. Тогда прямоугольник будет перемещаться вертикально сверху вниз.

```qml
NumberAnimation on y { from:10; to: 340; duration: 4000}
```
__Анимация цвета__ также является частным случаем анимации свойства `PropertyAnimation`.
Для создания анимации цвета в QML используется компонент `ColorAnimation`. 

```qml
Rectangle {
    id: myRect
    width: 100
    height: 100
    color: "red"
    
    ColorAnimation {
        id: colorAnim
        target: myRect
        property: "color"
        to: "blue"
        duration: 1000
    }
    
    MouseArea {
        anchors.fill: parent
        onClicked: colorAnim.start()
    }
}
```

В этом примере создан прямоугольник с красным фоном, к нему добавлен компонент `ColorAnimation`. Свойство `target` указывает на целевой элемент, свойство `property` указывает на анимируемое свойство, в данном случае это color. Свойство `to` задает конечное значение цвета, к которому должен измениться цвет фона элемента, а свойство `duration` задает время, которое будет затрачено на анимацию в миллисекундах.
`MouseArea` позволяет при клике на прямоугольник запускать анимацию изменения цвета. Когда пользователь кликает на элемент, вызывается метод `start()` анимации colorAnim, и фон прямоугольника начинает менять цвет с красного на синий в течение одной секунды.

Помимо этих основных и широко используемых элементов анимации, Qt Quick также предоставляет более специализированные анимации для конкретных случаев использования:

*	`PauseAnimation` – останавливает анимацию;
*	`SequentialAnimation` – позволяет запускать анимации последовательно;
*	`ParallelAnimation` – позволяет запускать анимации параллельно;
*	`AnchorAnimation` – анимирует изменения значений привязки;
*	`ParentAnimation` – анимирует изменения в родительских значениях;
*	`SmoothedAnimation` – позволяет свойству плавно отслеживать значение;
*	`SpringAnimation` – позволяет свойству отслеживать значение в пружинном движении;
*	`PathAnimation` – анимирует элемент вдоль пути;
*	`Vector3dAnimation` – анимирует изменения значений QVector3d.

Закон изменения скорости анимации задают переходные кривые, предназначеные для управления поведением скорости анимации в течение установленного периода. Виды кривых подробно описаны в официальной [документации](https://doc.qt.io/qt-5.6/qml-qtquick-propertyanimation.html#easing-prop).

Кривые для анимации устанавливают в свойстве `easing.type` анимационного объекта. 

```qml
PropertyAnimation{
    id:anim
    target:rect_anim; property: "x";from:10; to: 240
    duration: 4000
    running:true
    easing.type: "InExpo"
}
```

Сгруппируем перемещения прямоугольников из рассмотренных примеров с помощью `SequentialAnimation` в последовательность анимаций, выполняемых друг за другом.

```qml 
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    
    minimumWidth: 400
    minimumHeight: 450
    
    Rectangle{
        id:rect
        width:300; height:400
        anchors.centerIn:parent
        color:"lightgrey"
        
        Rectangle{
            id:rect_anim
            width:50; height:50
            border.color:"black"; border.width:1;
            color:"yellow"
            x:10; y:10
        }
        
        SequentialAnimation{
            id:anim
            NumberAnimation { target:rect_anim; property: "x";from:10; to: 240; duration: 4000}
            NumberAnimation { target:rect_anim; property: "y";from:10; to: 340; duration: 4000}
        }
    }
    
    MouseArea{
        anchors.fill:parent
        onClicked: anim.running=true
    }
}
```

Для описанного выше примера изменим вид группировки анимации с последовательной на параллельную, изменив только название:

```qml
ParallelAnimation{
    id:anim
    NumberAnimation { target:rect_anim; property: "x";from:10; to: 240; duration: 4000}
    NumberAnimation { target:rect_anim; property: "y";from:10; to: 340; duration: 4000}
}
```

В следующем примере приведен вариант реализации анимации изменения цвета прямоугольника. В простейшем виде это может выглядеть как однократное изменение цвета.

```qml
Rectangle { color: "green" ColorAnimation on color { to: "red" duration: 1000 } }
```
```qml
Rectangle {
    width: 200
    height: 200
    color: "red"
    
    Timer {
        interval: 2000
        repeat: true
        running: true
        onTriggered: {
            colorAnim.start()
        }
    }
    
    ColorAnimation {
        id: colorAnim
        target: parent
        property: "color"
        to: "blue"
        duration: 1000
        easing.type: InOutCubic
        
    }
}
```

Всегда следует помнить, что анимационные эффекты достаточно сильно привлекают внимание. Поэтому анимация должна быть тщательно проработана, чтобы поддерживать взаимодействие с пользовательским интерфейсом, а не доминировать над ним, отвлекая пользователя от решения его задач с помощью данного приложения.

Описание свойств типа `Animation` есть в официальной [документации](https://doc.qt.io/qt-5.6/qml-qtquick-animation.html).

## Состояния

Практически все пользовательские интерфейсы имеют конфигурации, которые различаются в зависимости от текущего состояния. Например, у светофора в состоянии `stop` загорится красный свет, а желтый и зеленый будут выключены; в состоянии `go` горит зеленый, красный и желтый выключены; в состоянии `caution` горит желтый, остальные выключены. 

Динамика пользовательского интерфейса как раз и описывается с помощью состояний и переходов. Интерфейс можно представить в виде графа, в котором узлами являются состояния, а ребрами — переходы между ними. Декларативный стиль языка QML направлен на то, чтобы избежать ошибок в построении интерфейса, которые вызываются некорректным переходом из одного состояния в другое. К переходам можно привязывать некоторые анимационные эффекты.

Состояния в QML определяются с помощью элемента `State`, который необходимо привязать к массиву `states` любого элемента `Item`. Состояния представляют собой набор значений свойств, определенных в типе `State`. Смена состояний влечет смену связанных с ним свойств, меняя конфигурацию интерфейса. Различные конфигурации могут, например:

*	показывать некоторые компоненты пользовательского интерфейса и скрывать другие;
*	предоставлять пользователю различные доступные действия;
*	запускать или останавливать анимацию;
*	выполнять некоторый скрипт, требуемый в новом состоянии;
*	менять конфигурацию якорей;
*	показывать другой вид или экран.

Каждый элемент, независимо от того, визуальный он или нет, может иметь состояния. Для набора состояний используют свойство `states`. Текущее состояние описывают свойством `state`. Состояние определяет набор изменений свойств `PropertyChanges` и может быть вызвано определенным условием. Состояние идентифицируется по имени и в своей простейшей форме состоит из последовательности изменений свойств элементов. Состояние по умолчанию определяется начальными свойствами элемента и имеет имя "" (пустая строка). 
Изменение свойства `PropertyChanges` посредством `target` задает описываемый объект, затем следует свойство и устанавливаемое для него значение свойства.

Рассмотрим описание и изменение состояний на примере светофора.

![](../../resources/84.png)

Заготовка под светофор:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 200
    height: 480
    minimumWidth: 300
    minimumHeight: 450
    visible: true
    title: qsTr("Hello World")
    
    Rectangle{
        id:rect
        width:200; height:400
        anchors.centerIn:parent
        color:"lightgrey"
        
        Rectangle{
            id:rect_yellow
            width:100
            height:100
            border.color:"black"
            border.width:1
            anchors.centerIn:rect
            color:"yellow"
        }
        Rectangle{
            id:rect_red
            width:100
            height:100
            border.color:"black"
            border.width:1
            x:rect_yellow.x; y:rect_yellow.y-rect_yellow.height-10
            color:"red"
        }
        Rectangle{
            id:rect_green
            width:100
            height:100
            border.color:"black"
            border.width:1
            x:rect_yellow.x; y:rect_yellow.y+rect_yellow.height+10
            color:"green"
        }
    }
}
```

Теперь вводим состояния. Состояниями будет обладать элемент `id:rect`.  Состояния определим через `states [  ]`, как список (перечень отдельных состояний через запятую). Первое состояние назовем `stop`. Описываем задействованные объекты через `target`  и определяем свойства цвета для этих объектов. Аналогично описываются состояния `caution` и `go`. Проверяем отображение состояний, задавая поочередно состояния "stop", "caution", "go".`

```qml
states:[
State {
    name: "stop"
    PropertyChanges {target: rect_red; color:"red"}
    PropertyChanges {target:rect_yellow;color: "black"}
    PropertyChanges {target:rect_green; color:"black"}
},

State {
    name: "caution"
    PropertyChanges {target: rect_red; color:"black"}
    PropertyChanges {target:rect_yellow;color: "yellow"}
    PropertyChanges {target:rect_green; color:"black"}
},

State {
    name: "go"
    PropertyChanges {target: rect_red; color:"black"}
    PropertyChanges {target:rect_yellow;color: "black"}
    PropertyChanges {target:rect_green; color:"green"}
}   
]

```

![](../../resources/85.png)

Добавляем обработчик события, которое будет менять состояние. Например, будем отслеживать клик мыши. Определяем `MouseArea`:

```qml
MouseArea{
    anchors.fill:parent
    onClicked: {
        if (parent.state == "stop") {
            parent.state = "caution";
        } else if (parent.state == "caution") {
            parent.state = "go";
        } else {
            parent.state = "stop";
        }
    }
}
```

Теперь при клике мыши меняется состояние. 

Определяем переходы для этого примера. В каждом элементе, помимо состояний (`states`), можно определить и переходы (`transitions`). Переходы, как и состояния, определяются списком. Внутри каждого перехода указываем, из какого состояния в какое производится переход, и подключаем анимационный эффект смены цвета. 

```qml
transitions:[
Transition {
    from: "stop"
    to: "caution"
    PropertyAnimation{ target: rect_red;properties:"color";duration:1000}
},
Transition {
    from: "caution"
    to: "go"
    PropertyAnimation{ target: rect_yellow;properties:"color";duration:1000}
},
Transition {
    from: "go"
    to: "stop"
    PropertyAnimation{ target: rect_green;properties:"color";duration:1000}
}
]
```

В строках настройки анимации `target` определяет объект для анимации, `properties` — свойства объекта, которые будут анимированы, `duration` — длительность анимационного эффекта в миллисекундах.

Таким образом реализованы переходы состояний с плавной анимацией смены цвета.

Состояние можно связать с некоторым логическим выражением с помощью конструкции `when`, и если это логическое выражение принимает значение `true`, то выполняется переход и, если необходимо, запускается анимация, связанная с этим переходом. Рассмотрим на примере. Для этого создадим текстовое поле и прямоугольник в качестве кнопки для очищения строки.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    minimumWidth: 400
    minimumHeight: 450
    visible: true
    title: qsTr("Hello World")
    
    Rectangle{
        id:rect
        width:300; height:400
        anchors.centerIn:parent
        color:"lightgrey"
        
        Rectangle{
            id:rect_button
            width:120
            height:80
            border.color:"black"
            border.width:1
            anchors.centerIn:parent
            color:"yellow"}
        TextInput{
            id: text_field
            text: "some text" }
        
        MouseArea{
            anchors.fill:rect_button
            onClicked: text_field.text=""
        }
    }
}
```

Добавляем состояния. Если в строке есть текст, кнопка очищения видима. Если текста нет, кнопка прозрачна.

```qml
states:[
State{
    name:"with_text"
    when: text_field.text !=""
    PropertyChanges{target:rect_button;opacity:1}
},
State{
    name:"with_out_text"
    when: text_field.text !=" "
    PropertyChanges{target:rect_button;opacity:0.25}
    PropertyChanges{target:text_field;focus:true}
}
]
```

На этих примерах видно, что с помощью описанных состояний можно управлять поведением элементов интерфейса.

Подробное описание свойств типа State в официальной [документации](https://doc.qt.io/qt-5.6/qml-qtquick-state.html).

Описание свойств типа Transition в официальной [документации](https://doc.qt.io/qt-5.6/qml-qtquick-transition.html).

__Дополнительный материал__

[Удобный интерфейс с помощью движения: 12 принципов UX-анимации](https://vk.com/@prosmotr-udobnyi-interfeis-s-pomoschu-dvizheniya-12-principov-ux-anim).
