# Прототипирование в Figma

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Прототипирование — создание черновой реализации будущей системы. Во время создания прототипов закладывается функциональность, создаются ссылки, наглядно оценивается удобство сценариев, продуманных в UserFlow.
Рассмотрим пример создания прототипа мобильного приложения. 

После регистрации в Figma при выборе тарифа Free доступно создание трех проектов. После создания проекта его можно переименовать.

![](../../resources/10.png)

## Фреймы

Создание дизайн-макета начинается с создания фрейма. Зачастую фрейм — это отдельный экран приложения.

![](../../resources/11.png)

В разделе Prototype будут предложены размеры фреймов для целого списка типов устройств. Можно также растянуть на экране фрейм произвольного размера под свое устройство.

Размер фрейма можно увидеть в панели Design.

![](../../resources/12.png)

Количество фреймов в проекте может быть любым. Можно создавать новые фреймы, можно копировать уже имеющиеся и вносить некоторые изменения. Список фреймов отображается в панели слоев. Там фрейм может быть переименован, скрыт из видимости ( но не удален из проекта) или заблокирован от изменений.

![](../../resources/13.png)

## Интерфейсная сетка

Сетка в интерфейсе — это инструмент, который позволяет быстро и точно располагать элементы в плоскости макета. Настраивается сетка в группе инструментов Layout grid.

![](../../resources/14.png)

Размер ячейки сетки можно менять. Но прямоугольная сетка не очень удобна для проектирования интерфейсов. для дизайна сайтов и мобильных приложений часто используют колоночную сетку.

![](../../resources/15.png)

 Количество колонок должно позволять удобно располагать содержимое экранов и способствовать его выравниванию. Один из самых удобных вариантов — использование сетки в 12 колонок. Это позволяет размещать на экране и четное и нечетное количество элементов интерфейса.

![](../../resources/16.png)

Но для разных фреймов можно настраивать разные сетки, так как содержимое экранов различно. Важно лишь помнить про консистентность в дизайне. Согласованность в сетках помогает достигать одинаковых отступов от краев экрана (offsets) в макете, а также равных или пропорциональных межколонных расстояний (gutters) между столбцами. 

В требованиях к полям в гайдлайнах для Android и iOS минимальные рекомендуемые поля макета — 16pt. Если придерживаться стандартных рекомендаций, то поля не должны быть меньше.При этом межколонный интервал нужно подсчитать пропорционально модулю 8pt. Такой подход обеспечит единый горизонтальный и вертикальный ритм в макете.

В iOS и Android многие экраны имеют нечетную ширину и не делятся нацело без остатка. Можно уменьшить правое поле на один пиксель, а оставшееся пространство разделить на нужное количество столбцов, либо создавать сетки внутри отдельных компонентов.

К существующей сетке при необходимости можно добавить дополнительную, нажав на знак “+”.

![](../../resources/17.png)

Видимость сетки может быть отключена.

Для файлов совместимых с популярными графическими инструментами: Adobe XD, Photoshop, Illustrator, Sketch и Figma имеется [библиотека популярных систем сеток для iOS, Android](https://www.pixsellz.io/grid-system-library). 

## Добавление объектов

Во фрейм могут быть добавлены формы, такие как прямоугольник, линии, эллипс и др.

![](../../resources/18.png)

Объекты, внедренные во фрейм, являются его дочерними элементами и в палитре слоев сдвинуты вправо.

![](../../resources/19.png)

Объект, вставленный позже, отрисовывается поверх вставленного ранее.
У объектов можно менять настройки, влияющие на внешний вид: скругление углов, обводка, заливка, тень и др.

Если одновременно выделены несколько объектов, для них доступны варианты логической группировки: сложения, вычитания, пересечения.

![](../../resources/20.png)

Выделенные объекты могут быть быстро выровнены по вертикали или горизонтали.

![](../../resources/21.png)

Результат выравнивания по левому вертикальному краю:
 
![](../../resources/22.png)

Результат выравнивания по верхнему горизонтальному краю:

![](../../resources/23.png)

Также между выделенными элементами можно быстро установить равное расстояние:

![](../../resources/24.png)

Результат:

![](../../resources/25.png)

## Слои. Auto Layout

Каждый добавляемый объект во фрейме размещается на своем слое. Если в макете присутствует некоторая группа схожих элементов, есть смысл создать сгруппированную основу для ее клонирования (копирования). Для этого входящие в группу элементы выделяются и объединяются через команду Group selection контекстного меню, либо сочетанием клавиш Ctrl+G.

![](../../resources/26.png)

Поскольку в макете группы не являются полностью идентичными и отличаются по меньшей мере контентом, то некоторые их параметры (длина, ширина, и др) могут отличаться от исходного. Но при этом, для сохранения стилевого единства, другие параметры, например отступы, должны оставаться одинаковыми. В этом случае используют настройки AutoLayout. Инструмент Auto Layout в Figma позволяет указывать отступы и выравнивать соседние модули автоматически.

Для создания адаптивной кнопки выполните следующие шаги.

1.	Напишите любой текст.
2.	На панели слоёв нажмите на текст правой кнопкой мыши и выберите Frame Selection.

	![](../../resources/27.png)

3.	Выделите получившийся фрейм, на панели настроек добавьте ему фон в блоке Fill и нажмите на + рядом с Auto Layout.
4.	Выставьте отступы. 

	![](../../resources/28.png)

	Можно выставить индивидуальные отступы для каждой стороны. 

В итоге получилась кнопка, которая автоматически подстраивается под длину текста.

![](../../resources/29.gif)

Кнопка сама адаптируется под длину текста благодаря параметру Resizing — изменение размера. По умолчанию Figma в Resizing использует свойство Hug contents — форма фрейма с активной функцией Auto Layout подстраивается под длину и высоту содержимого.

## Параметры Auto Layout на панели настроек

Стрелки указывают, как будут выстраиваться элементы внутри фрейма с Auto Layout. Стрелка ↓ означает вертикальное выравнивание, а → — горизонтальное.

*	Отступы между элементами:

![](../../resources/29.png)

*	Внешние поля:

![](../../resources/30.png)

![](../../resources/31.png)

Выравнивание объектов 
и настройка их расположения во фрейме: 

*	Packed — элементы будут стоять рядом друг с другом; 
*	Space between — равномерно распределятся по всему фрейму с Auto Layout.

![](../../resources/32.png)

## Стили

Стили: это многократно используемые коллекции свойств, которые вы можете легко применять к элементам вашего дизайна. В Figma можно создавать стили для текста, цветов, сеток и эффектов, таких как тени и размытия.

Настройки стиля прячутся за специальной иконкой. После нажатия на нее появятся уже доступные стили (если они есть), а также возникнет возможность создать новый с помощью кнопки + в появившемся окне.

![](../../resources/33.png)

Добавляется стиль, его название и описание:

![](../../resources/34.png)

Если в названии стиля использовать слеш, то можно вводить название группы, подгруппы, затем самого стиля:

*	Button / Primary / Default
*	Button / Primary / Hover
*	Button / Primary / Click

![](../../resources/35.png)

Мы получим группу Button с подгруппой Primary с цветами, которые удобно использовать для различных состояний кнопок.

Аналогично вводятся стили для текста.

![](../../resources/36.png)

Настраивать иерархию стилей удобно не только для цветов, но и для типографики.

## Компоненты

 Компонент в Figma – это один из элементов интерфейса. Это элемент, копии которого меняются вместе с ним. Исходный компонент называется основным (main component), его копии — экземплярами (instances). 

Компоненты позволяют создавать повторяющиеся элементы интерфейса, такие как кнопки, формы, иконки и т.д., и повторно использовать их в других частях разрабатываемого проекта. Это позволяет ускорить процесс дизайна, улучшить его эффективность и упростить поддержку и обновление дизайна.

Рассмотрим процесс использования компонента.
Создаем компонент на основе прямоугольника.

![](../../resources/37.png)

Вставим пару копий этого компонента. В палитре слоев у копий отображается значок пустого ромба. 

![](../../resources/38.png)

Если перекрасить компонент, то перекрасятся и его копии, т.к. экземпляры наследуют параметры основного компонента. Наряду с наследованием, экземпляры можно изменять напрямую, не разрывая при этом связь с «родителем». Например, перекрасить второй квадрат, а третьему изменить размер. Свойства, полученные экземплярами непосредственно, всегда приоритетны по сравнению с унаследованными. 

Основной компонент — шаблон для будущих элементов дизайна.

## Анимации в Figma. Кликабельный прототип

Анимация помогает делать кликабельные прототипы. Для это отрисовывается экран с начальной и конечной точкой анимации, между ними создается связь и описывается характер анимации (мгновенная, растворение, перемещение внутрь или наружу). При этом анимации можно задать степень плавности Easing и длительность. Длительность задается в миллисекундах (1000 мс = 1с)

Плавность помогает сделать движение в сложных анимациях более реалистичным и за счет этого привлечь большее внимание пользователя. Наиболее популярные сценарии: ease-in ― ускорение и ease-out ― замедление.

Постоянная скорость используется, например, в простых вспомогательных анимациях, построенных на изменении цвета и прозрачности. Они не должны сильно отвлекать пользователя: их основная задача ― органично дополнять дизайн, поэтому заострять на них внимание с помощью плавности не стоит.

Настраивать кликабельный прототип и прописывать реакции системы очень помогает разработанный заранее User Flow. Настройка переходов начинается с переключения вкладки Design на Prototype. Выбрав элемент, с которым будет взаимодействовать пользователь, захватываем появившийся кружок и перетаскиваем его на тот фрейм, в котором будет отражено новое состояние системы.

Cпособ реализации ховера:

1.	Создаём дубль исходного фрейма. 
2.	Перекрашиваем фон кнопки. 
3.	Настраиваем переход с первого фрейма на второй на событие Mouse Enter.
4.	Настраиваем переход со второго фрейма на первый на событие Mouse Leave.
![](../../resources/89.png)

Не существует единых правил для оформления анимации в техническом задании. Настраивая переходы системы из одного состояния в другое, важно следить за тем, чтобы были реализованы проходы пользователя по его задачам и не было тупиковых состояний интерфейса (с входом, но без выхода).

__Бесплатные курсы по изучению работы в Figma:__

*	[Основы Figma от Нетологии](https://netology.ru/programs/osnovy-figma?utm_source=yandex&utm_medium=cpc&utm_campaign=brand_all_bou_ya_search_dynamic_site_rf_related&utm_term=&utm_content=k50id%7C010000004120524_4120524%7Ccid%7C78801585%7Cgid%7C5232432432%7Caid%7C14542248370%7Cadp%7Cno%7Cpos%7Cpremium1%7Csrc%7Csearch_none%7Cdvc%7Cdesktop%7Cmain&etext=&yclid=3944450214782898221#!%26_escaped_fragment_=)

*	[Бесплатный пробный курс по Фигме](https://www.youtube.com/playlist?list=PLM2Q6lcZo4MexclJrYxA0Is42qWBBuHpB)
*	[Дизайн мобильного приложения в Figma](https://www.youtube.com/playlist?list=PLZsq8DhHboqy_MthqipI5q02M3seeHZhM)
