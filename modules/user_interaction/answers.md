# Возможные ответы на задания по теме «Изменение свойств объектов при взаимодействии с пользователем»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Обработка сигналов

![](../../resources/109.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
Window {
    width: 360
    height: 640
    visible: true
    title: qsTr("Task_for_Signal")
    Rectangle {
        id:fon
        anchors.fill:parent
        color: "white smoke"
        
        Rectangle {
            id:header
            height:60
            anchors.left:fon.left
            anchors.right:fon.right
            anchors.top:fon.top
            color: "gainsboro"
            Text{ id:header_txt;text:"Header";anchors.centerIn: parent}
        }
        Rectangle {
            id:cont
            anchors.top:header.bottom
            anchors.bottom:footer.top
            anchors.left: fon.left
            anchors.right: fon.right
            anchors.margins: 10
            color: "white smoke"
            border.color:"silver"
            Text{ id:body_txt; text:"Some content";anchors.centerIn: cont}
        }
        
        Rectangle{
            id:footer
            height:60
            anchors.left:fon.left
            anchors.right:fon.right
            anchors.bottom:fon.bottom
            color: "gainsboro"
            
            RowLayout{
                anchors.left:footer.left
                anchors.right:footer.right
                spacing:16
                
                Rectangle{
                    id:btn1;width:100; height:60;
                    color:"light grey";border.color:"silver";border.width: 1
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{id:txt1;text:"Item 1";anchors.centerIn: parent}
                    MouseArea{anchors.fill: parent
                        onClicked: {
                            btn1.opacity=1
                            btn2.opacity = 0.4
                            btn3.opacity = 0.4
                            body_txt.text="Item 1 content"
                            header_txt.text="Header 1"
                        }
                    }
                }
                
                Rectangle{
                    id:btn2;width:100; height:60;
                    color:"light grey";border.color:"silver";border.width: 1
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{id:txt2;text:"Item 2";anchors.centerIn: parent}
                    MouseArea{anchors.fill: parent
                        onClicked: {
                            btn1.opacity=0.4
                            btn2.opacity = 1
                            btn3.opacity = 0.4
                            body_txt.text="Item 2 content"
                            header_txt.text="Header 2"
                        }
                    }
                }
                
                Rectangle{
                    id:btn3;width:100; height:60;
                    color:"light grey";border.color:"silver";border.width: 1
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{id:txt3;text:"Item 3";anchors.centerIn: parent}
                    MouseArea{anchors.fill: parent
                        onClicked: {
                            btn1.opacity=0.4
                            btn2.opacity = 0.4
                            btn3.opacity = 1
                            body_txt.text="Item 3 content"
                            header_txt.text="Header 3"
                        }
                    }
                }
            }
        }
    }
}
```
