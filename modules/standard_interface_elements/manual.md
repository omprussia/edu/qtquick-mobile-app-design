# Элементы управления модуля QtQuick.Controls

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Для набора элементов управления пользовательского интерфейса модуль [Qt Quick Controls](https://doc.qt.io/qt-5/qtquickcontrols-index.html) реализует несколько элементов управления: кнопки, меню и представления. Они имеют несколько встроенных стилей, которые можно использовать, а также поддерживают создание пользовательских стилей.

## ApplicationWindow. Окно приложения.

Свойства для кастомизации и настройки окна приложения:

*	`background` — фон.

```qml
import QtQuick 2.15
import QtQuick.Controls

ApplicationWindow {
    visible: true
    
    background: Rectangle {
        gradient: Gradient {
            GradientStop { position: 0; color: "#ffffff" }
            GradientStop { position: 1; color: "#c1bbf9" }
        }
    }
}
```

Эта программа создает окно приложения с фоном, который имеет градиентную заливку, начинающуюся с белого цвета в верхней части фона и заканчивающейся с фиолетовым цветом в нижней части.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-window-window.html).
 
## Button

В QML элемент `Button` имеет ряд свойств, которые можно использовать для настройки внешнего вида и поведения кнопки. Некоторые из свойств, которые можно установить:

*	`text` — текст, отображаемый на кнопке;
*	`font` — шрифт текста на кнопке;
*	`color` — цвет текста на кнопке;
*	`background` — фон кнопки;
*	`enabled` — свойство, указывающее, может ли кнопка быть нажата (по умолчанию true);
*	`checkable` — свойство, указывающее, может ли кнопка быть переключаемой (по умолчанию false);
*	`checked` — свойство, указывающее, включена ли кнопка (по умолчанию false);
*	`onClicked` — сигнал, который генерируется при нажатии на кнопку.

У кнопки есть два объекта для настройки стиля — непосредственно сама прямоугольная область и текст, заключенный внутри нее. Поэтому QML предоставляет нам два соответствующих свойства:

*	`background` — область фона;
*	`contentItem` — содержимое.

В приведенном ниже примере все пространство окна занимает светло-серый прямоугольник. Элемент `Button` центрирован относительно него и зависит от размеров этого прямоугольника. Свойство `contentItem` представляет текст, выровненный по вертикали и горизонтали кнопки с установками шрифта на размер, цвет и полужирность. Свойство `background` представлено прямоугольником со скругленными краями и серой границей. Цвет прямоугольника определяется тернарным выражением в зависимости от состояния.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Button_test")
    
    Rectangle{
        anchors.fill:parent
        color:"lightgrey"
        
        Button{
            id:button_test
            anchors.centerIn: parent
            width:parent.width/3
            height:parent.height/4
            contentItem: Text{
                text: "Press me"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 14
                font.bold:true
                color:"white"
                
            }
            background: Rectangle{
                property string normalColor:"orange"
                property string hoverColor:"red"
                property string pressedColor:"darkred"
                radius:5
                
                color: button_test.pressed ? pressedColor:
                                             button_test.hovered ? hoverColor:
                                                                   normalColor
                
                border.color: "grey"
                border.width: 2
            }
        }
    }
}
```

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-button.html).

## CheckBox

`CheckBox` или флажок — элемент графического пользовательского интерфейса, позволяющий пользователю управлять параметром с двумя состояниями — ☑ включено и ☐ отключено. Во включенном состоянии внутри чекбокса отображается отметка (галочка (✓). По традиции флажок имеет квадратную форму. Рядом с флажком отображается его обозначение, обычно — подпись.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("CheckBox_test")
    
    CheckBox {
        id: testCheckBox
        anchors.centerIn: parent
        text: qsTr("Check me...")
    }
}
```

Взаимодействие с `CheckBox` заключается в проверке свойства `checkState`. Возможные значения зависят, в свою очередь, от свойства tristate. По умолчанию `tristate = false`, и `checkState` может принимать значения:

*	`Qt.Unchecked`;
*	`Qt.Checked`.

Если же `tristate = true`, то спектр вариантов пополняется еще одним:

*	`Qt.PartiallyChecked`.

Пример визуализации трех состояний.

![](../../resources/86.png)

```qml
Window {
    width: 320
    height: 240
    visible: true
    title: qsTr("CheckBox_test")
    CheckBox {
        id: testCheckBox
        anchors.centerIn: parent
        text: qsTr("Chack me...")
        tristate:true
        onCheckStateChanged: {
            switch (checkState) {
            case Qt.Unchecked:
                testCheckBoxState.text = "I'm unchecked";
                break;
            case Qt.PartiallyChecked:
                testCheckBoxState.text = "I'm partially checked";
                break;
            case Qt.Checked:
                testCheckBoxState.text = "I'm checked";
                break;
            default:
                break;
            }
        }
    }
    Text {
        id: testCheckBoxState
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: testCheckBox.bottom
        anchors.topMargin: 10
        text: qsTr("State")
        color:"red"
    }
}
```

Несколько элементов `CheckBox` можно группировать в элемент `ButtonGroup` и связывать их друг с другом при помощи свойства `ButtonGroup.group`. 

Теперь в поле `Text` будет выводиться состояние `ButtonGroup` через `primaryGroup.checkState`:

![](../../resources/88.png)

```qml
Window {
    width: 320
    height: 240
    visible: true
    title: qsTr("CheckBoxGroup_test")
    Column {
        id: mainColumn
        ButtonGroup {
            id: primaryGroup
            exclusive: false
        }
        CheckBox {
            text: qsTr("CheckBox 1")
            ButtonGroup.group: primaryGroup
        }
        CheckBox {
            text: qsTr("CheckBox 2")
            ButtonGroup.group: primaryGroup
        }
        CheckBox {
            text: qsTr("CheckBox 3")
            ButtonGroup.group: primaryGroup
        }
    }
    Text {
        id: primaryGroupState
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: mainColumn.bottom
        anchors.topMargin: 10
        text: qsTr("State") + " " + primaryGroup.checkState
        color:"red"
    }
}
```

Состояние `ButtonGroup` определяется состояниями всех добавленных к ней элементов:

*	Если состояние всех `CheckBox` — unchecked, то и `primaryGroup` - unchecked.
*	Если состояние всех `CheckBox` — checked, то `primaryGroup` - checked.
*	Если часть из `CheckBox` — checked, `primaryGroup`` — partially checked.

[Подробнее в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-checkbox.html).

## ComboBox

`ComboBox` или раскрывающийся список — элемент графического интерфейса пользователя, представляющий сочетание выпадающего списка (раскрывающегося при щелчке мыши) и однострочного текстового поля, которое позволяет пользователю ввести значение вручную или выбрать из списка.

Свойства для кастомизации `ComboBox`:

*	`background` — область фона;
*	`contentItem` — содержимое;
*	`indicator` — определяет, будет ли отображаться индикатор состояния Combobox (например, стрелка вниз), которая обычно находится рядом с текстовым полем `Combobox`;
*	`delegate` — определяет пользовательский элемент интерфейса, который будет использоваться для отображения каждого элемента в списке;
*	popup — определяет, будет ли отображаться выпадающий список, когда пользователь щелкает на `Combobox`.

```qml
import QtQuick 2.0
import QtQuick.Controls 2.0

ApplicationWindow {
    visible: true
    width: 200
    height: 100
    
    ComboBox {
        id: comboBox
        anchors.centerIn: parent
        model: ["Red", "Green", "Blue"]
        currentIndex: 0
        
        onActivated: {
            console.log("Selected color:", comboBox.currentText)
        }
    }
}
```

В этом примере создается Combobox с тремя цветами в качестве элементов списка. С помощью свойства `currentIndex` выбранный элемент устанавливается в качестве первого (Red). С помощью события `onActivated` выведется сообщение в консоль при выборе пользователем другого элемента.

При запуске приложения будет отображен Combobox со списком цветов и выбранным элементом "Red". При выборе другого элемента из списка будет выведено в консоль сообщение о выбранном цвете.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-combobox.html).

## Switch

`Switch` или переключатель — элемент интерфейса, который позволяет выбрать одно из состояний, чаще всего вкл/выкл.

Свойства для кастомизации и настройки стиля переключателя:

*	`background` — позволяет установить фоновое изображение или цвет для элемента `Switch`;
*	`contentItem` — содержимое элемента;
*	`indicator` — определяет внешний вид поля переключателя.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    
    Switch {
        id: control
        text: qsTr("Switch")
        
        indicator: Rectangle {  // Это поле переключателя
            implicitWidth: 48
            implicitHeight: 26
            x: control.leftPadding
            y: parent.height / 2 - height / 2
            radius: 13
            color: control.checked ? "#17a81a" : "#ffffff"
            border.color: control.checked ? "#17a81a" : "#cccccc"
            
            Rectangle {         // Это движущийся объект переключателя
                x: control.checked ? parent.width - width : 0
                width: 26
                height: 26
                radius: 13
                color: control.down ? "#cccccc" : "#ffffff"
                border.color: control.checked ? (control.down ? "#17a81a" : "#21be2b") : "#999999"
            }
        }
        
        contentItem: Text {       // Здесь подпись к переключателю
            text: control.text
            font: control.font
            opacity: enabled ? 1.0 : 0.3
            color: control.down ? "#17a81a" : "#21be2b"
            verticalAlignment: Text.AlignVCenter
            leftPadding: control.indicator.width + control.spacing
        }
    }
}
```
Эта программа на QML создает переключатель (`Switch`) с текстом, который может быть включен или выключен.

Когда переключатель включен, фоновый прямоугольник меняет цвет на зеленый, а квадрат, представляющий переключатель, перемещается в правую часть прямоугольника. Когда переключатель выключен, фоновый прямоугольник становится белым, а квадрат перемещается в левую часть прямоугольника.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-switch.html).

## Slider

![](../../resources/39.png)

`Slider` или ползунок — это элемент управления пользовательского интерфейса, который позволяет пользователю выбирать значение из числового диапазона, перетаскивая ползунок на шкале.

Свойства для кастомизации `Slider`:

*	`background` — свойство, определяющее внешний вид слайдера;
*	`handle` —  определяет внешний вид и поведение ползунка.


Пример кастомизации.
```qml
Slider {
    id: control
    anchors.centerIn: parent
    width: parent.width *	0.8
    height: parent.height *	0.3
    
    from: 0
    to: 100
    stepSize: 1
    value:50
    
    
    // Здесь кастомизируем поле ползунка
    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 4
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: "#bdbebf"
        
        
        Rectangle {
            width: control.visualPosition *	parent.width
            height: parent.height
            color: "#21be2b"
            radius: 2
        }
    }
    
    // Здесь кастомизируем сам ползунок
    handle: Rectangle {
        x: control.leftPadding + control.visualPosition *	(control.availableWidth - width)
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 26
        implicitHeight: 26
        radius: 13
        color: control.pressed ? "#bbbbbb" : "#f6f6f6"
        border.color: "#bdbebf"
    }
    // Здесь отображаем текущее значение
    Text {
        text: "Current Value: " + control.value.toFixed(0)
        anchors.top: control.bottom
        anchors.horizontalCenter: control.horizontalCenter
    }
}
```

Данная программа создает элемент пользовательского интерфейса, который позволяет пользователю выбирать значение в заданном диапазоне (от 0 до 100) с помощью ползунка, а также отображает выбранное значение в текстовом поле. В начальный момент времени ползунок установлен в середине диапазона (значение 50). Ориентация слайдера горизонтальная. Поле ползунка меняем цвет с серого на зеленый по мере увеличения выбранного значения. Сам ползунок меняет цвет со светло-серого на насыщенно серый при нажатии на него.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-slider.html).

## StackView

`StackView` — это компонент пользовательского интерфейса, который позволяет упорядочивать экраны приложения в виде стека. Он является частью инструментов разработки графического интерфейса Qt, который используется для создания кросс-платформенных приложений с графическим интерфейсом пользователя.

```qml
import QtQuick
import QtQuick.Controls

StackView {
    id: control
    
    popEnter: Transition {
        XAnimator {
            from: (control.mirrored ? -1 : 1) *	-control.width
            to: 0
            duration: 400
            easing.type: Easing.OutCubic
        }
    }
    
    popExit: Transition {
        XAnimator {
            from: 0
            to: (control.mirrored ? -1 : 1) *	control.width
            duration: 400
            easing.type: Easing.OutCubic
        }
    }
}
```

Эта программа описывает элемент` StackView` пользовательского интерфейса Qt Quick для создания стека вложенных экранов, которые могут быть легко переключены.

Параметр `popEnter` определяет анимацию, которая происходит при появлении нового экрана на вершине стека, а параметр `popExit` определяет анимацию, которая происходит при удалении верхнего экрана со стека.

В данном случае анимация определена с использованием `Transition` и `XAnimator`. При входе на экран (`popEnter`) анимация будет перемещать экран справа налево, показывая его, а при выходе (`popExit`) анимация будет перемещать экран слева направо, скрывая его. Продолжительность анимации составляет 400 миллисекунд, а тип анимации определяется как `Easing.OutCubic`, что означает плавное замедление анимации к концу каждой транзакции.

Значения параметров `from` и `to` определяют начальную и конечную позиции элемента во время анимации. Значение - `control.width` обеспечивает перемещение элемента за границу экрана, а значение `control.width` перемещает элемент обратно на экран.

Значение `control.mirrored` используется для определения направления анимации.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-stackview.html).

## PageIndicator

`PageIndicator` в QML — это элемент управления пользовательским интерфейсом, используемый обычно для отображения количества страниц или элементов в приложении, которые можно просмотреть или выбрать.

Свойства для кастомизации и настройки индикатора страниц:

*	`background` — область фона;
*	`contentItem` — содержание;
*	`delegate` — определяет пользовательский элемент для отображения индикатора для каждой страницы.

```qml
import QtQuick
import QtQuick.Controls

PageIndicator {
    id: control
    count: 5
    currentIndex: 2
    
    delegate: Rectangle {
        implicitWidth: 8
        implicitHeight: 8
        
        radius: width / 2
        color: "#21be2b"
        
        opacity: index === control.currentIndex ? 0.95 : pressed ? 0.7 : 0.45
        
        required property int index
        
        Behavior on opacity {
            OpacityAnimator {
                duration: 100
            }
        }
    }
}
```

Эта программа создает компонент `PageIndicator`, который используется для отображения индикаторов страниц в приложениях с несколькими страницами: карусели, страницы книги и т.д.

Атрибуты компонента задают количество страниц (`count`) и текущую страницу (`currentIndex`), которую нужно отображать.

В блоке `delegate` задается внешний вид индикаторов страниц. В данном случае они представлены прямоугольниками определенного размера и цвета. Операторы условия в блоке opacity определяют, должен ли индикатор быть непрозрачным (`opacity: 0.95`), если он соответствует текущей странице (`index === control.currentIndex`), или прозрачным (`opacity: 0.7`), если он был нажат, или полупрозрачным (`opacity: 0.45`) в противном случае.

Блок `required property int index` определяет для делегата свойство `index`, которое необходимо для правильной работы `PageIndicator`.

Блок `Behavior` определяет, какие анимации должны использоваться при изменении свойства `opacity`. В данном случае используется `OpacityAnimator` с длительностью 100 мс.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-pageindicator.html).

## MenuBar

`MenuBar` — это элемент, который позволяет создавать горизонтальное меню с различными элементами, такими как `Menu`, `MenuItem` и `MenuSeparator`.

Свойства для кастомизации и настройки меню:

*	`background` — область фона;
*	`content item` — содержание.

```qml
MenuBar {
    id: menuBar
    
    Menu { title: qsTr("File") }
    Menu { title: qsTr("Edit") }
    Menu { title: qsTr("View") }
    Menu { title: qsTr("Help") }
    
    delegate: MenuBarItem {
        id: menuBarItem
        
        contentItem: Text {
            text: menuBarItem.text
            font: menuBarItem.font
            opacity: enabled ? 1.0 : 0.3
            color: menuBarItem.highlighted ? "#ffffff" : "#21be2b"
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        
        background: Rectangle {
            implicitWidth: 40
            implicitHeight: 40
            opacity: enabled ? 1 : 0.3
            color: menuBarItem.highlighted ? "#21be2b" : "transparent"
        }
    }
    
    background: Rectangle {
        implicitWidth: 40
        implicitHeight: 40
        color: "#ffffff"
        
        Rectangle {
            color: "#21be2b"
            width: parent.width
            height: 1
            anchors.bottom: parent.bottom
        }
    }
}
```

Данный код создает `MenuBar` с четырьмя пунктами: "File", "Edit", "View" и "Help". Каждый пункт меню создается с помощью элемента `Menu`.

Далее задается `delegate` — элемент, который определяет, как должен выглядеть каждый пункт. В данном случае используется `MenuBarItem`, который содержит `Text` элемент для отображения текста пункта меню и `Rectangle `элемент для фона. При выборе пункта меню его фон окрашивается в зеленый цвет.

Также задается background — элемент, который определяет фон `MenuBar`. Элемент `background` содержит `Rectangle` элемент, который определяет размер и цвет фона `MenuBar`, а также нижнюю границу с зеленой линией.

Таким образом, данная программа создает простое меню, в котором можно выбирать различные пункты. Каждый пункт имеет зеленый фон при выборе, а само меню имеет белый фон с зеленой нижней границей. 

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-menubar.html).

## TabBar

`TabBar`  — это элемент управления пользовательским интерфейсом, который обычно используется для создания вкладок в приложении. Он позволяет пользователю переключаться между различными представлениями или страницами приложения, отображая заголовки на вкладках.

Свойства для кастомизации и настройки переключателя вкладок:

*	`background` — область фона;
*	`contentItem` — содержание.

```qml
import QtQuick 2.6
import QtQuick.Controls 2.1

TabBar {
    id: control
    
    background: Rectangle {
        color: "#eeeeee"
    }
    
    TabButton {
        text: qsTr("Home")
    }
    TabButton {
        text: qsTr("Discover")
    }
    TabButton {
        text: qsTr("Activity")
    }
}
```

Данная программа создает `TabBar` с тремя вкладками: "Home", "Discover" и "Activity". Каждая вкладка создается с помощью элемента `TabButton`, в который передается текстовый заголовок с помощью свойства `text`.

Также в программе задается цвет фона `background` для `TabBar` с помощью элемента `Rectangle`.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-tabbar.html).

## TextArea. Многострочная область ввода текста

Свойства для кастомизации и настройки области ввода:

*	`background` — область фона.

```qml
import QtQuick 2.6
import QtQuick.Controls 2.1

TextArea {
    id: control
    placeholderText: qsTr("Enter description")

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 40
        border.color: control.enabled ? "#21be2b" : "transparent"
    }
}
```

Эта программа на QML создает текстовое поле, которое позволяет пользователю вводить текст. Оно имеет следующие свойства:

*	`placeholderText: qsTr("Enter description")` — текст, отображаемый внутри текстового поля в качестве подсказки, помогает пользователю понять, что нужно ввести;
*	`background: Rectangle` — задает фон для текстового поля в виде прямоугольника;
*	`implicitWidth: 200` — задает ширину фона по умолчанию, если она не была явно указана;
*	`implicitHeight: 40` — задает высоту фона по умолчанию, если она не была явно указана;
*	`border.color: control.enabled ? "#21be2b" : "transparent"` — задает цвет границы для текстового поля. 

Если `control` включен (то есть текстовый блок активен для редактирования), граница будет зеленой (#21be2b). Если `control` выключен (недоступен для редактирования), граница будет прозрачной.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-textarea.html).

## TextField. Однострочное поле ввода текста

Свойства для кастомизации и настройки поля ввода:

*	`background` — область фона.

Настраивается аналогично TextArea.

```qml
import QtQuick 2.6
import QtQuick.Controls 2.1

TextField {
    id: control
    placeholderText: qsTr("Enter description")

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 40
        color: control.enabled ? "transparent" : "#353637"
        border.color: control.enabled ? "#21be2b" : "transparent"
    }
}
```

Данная программа создает текстовое поле с определенными свойствами:

*	`placeholderText` устанавливает текст-подсказку, которая отображается в текстовом поле, когда оно пустое;
*	`background` определяет фоновый элемент, который будет отображаться вокруг текстового поля. 

В данном случае это прямоугольник (`Rectangle`), у которого заданы размеры (`implicitWidth` и `implicitHeight`) и цвет (`color`). Если текстовое поле доступно для ввода, то фоновый прямоугольник будет прозрачным, а границы — зелеными (`border.color`). Если текстовое поле заблокировано, цвет границ будет прозрачным, а цвет фона — темно-серым (#353637).

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-controls2-textfield.html).

## TextInput

`TextInput` — это элемент управления, который позволяет пользователю вводить текст. Он может использоваться для получения ввода от пользователя, например, для ввода имени или сообщения.

Свойства для кастомизации и настройки поля ввода:

*	`background` — область фона.

`TextInput` и `TextField` — это два элемента управления в QML для ввода текста, но они имеют некоторые отличия в своем функционале и использовании.

Основное отличие между `TextInput` и `TextField` заключается в том, что `TextInput` более низкоуровневый элемент, который предоставляет больше свободы в настройке внешнего вида и функциональности, в то время как `TextField` предоставляет более удобный API для быстрой настройки часто используемых функций.

`TextInput` позволяет пользователю вводить произвольный текст, а также обрабатывает нажатия клавиш, перемещения курсора и выделения текста. Он также позволяет настраивать отображение вводимого текста. 

Установив свойство `echoMode` в значение `TextInput.Password`, можно адаптировать внешний вид поля для ввода пароля, когда введенные символы заменяются звездочками.

[Подробная информация в официальной документации](https://doc.qt.io/qt-5/qml-qtquick-textinput.html).
