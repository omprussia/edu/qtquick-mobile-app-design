# Возможные ответы на задания по теме «Элементы управления модуля QtQuick.Controls»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание 1

![](../../resources/111.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Window {
   width: 360
   height: 640
   visible: true
   title: qsTr("Task_for_Login_Page")
   Rectangle{
       id:fon
       anchors.fill:parent
       color: "white smoke"
      
       Column {
           spacing: 20
           anchors.centerIn: parent
          
           TextField {
               id: usernameField
               placeholderText: "Username"
               font.pixelSize: 16
           }
          
           TextField {
               id: passwordField
               placeholderText: "Password"
               font.pixelSize: 16
               echoMode: TextInput.Password
           }
          
           RowLayout{
               spacing:30
               Button {
                   text: "Log In"
                   font.pixelSize: 16
                   onClicked: {}
               }
               Button {
                   text: "Clear"
                   font.pixelSize: 16
                   background:  Rectangle{
                       Layout.fillWidth: parent
                       color:"white smoke"}
                   onClicked: {}
               }
            }
       }
    }
}
```

## Задание 2

![](../../resources/112.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
Window {
    
    width: 360
    height: 640
    visible: true
    title: qsTr("Task_for_Login_Page")
    Rectangle{
        id:fon
        anchors.fill:parent
        color: "white smoke"
        
        ColumnLayout {
            spacing: 20
            anchors.centerIn:fon
            
            Text {
                text: "Enter your password:"
                font.pixelSize: 16
                Layout.alignment: Qt.AlignCenter
            }
            Rectangle {
                id: passwordField1
                color: "white"
                border.width: 2
                border.color: "black"
                width: parent.width
                height: 50
                Layout.alignment: Qt.AlignCenter
                
                Row {
                    spacing: 6
                    anchors.centerIn: parent
                    
                    // Добавляем 6 элементов Label для отображения введенных символов
                    Repeater {
                        model:6
                        Label {
                            width: 20
                            height: 20
                            font.pixelSize: 36
                            text:  "*"
                            Layout.alignment: Qt.AlignCenter
                            color:index <passwordField.text.length ? "black" : "light grey"
                        }
                    }
                }
            }
            
            Text{
                id:passwordField
                text:passwordField.text
                color: fon.color // формируемый пароль, невидим пользователю (цвет фона)
            }
            
            GridLayout {
                id: keypad
                rows: 4
                columns: 3
                width: parent.width
                
                Button {
                    text: "1"
                    onClicked: passwordField.text += "1"
                }
                
                Button {
                    text: "2"
                    onClicked: passwordField.text+= "2"
                }
                
                Button {
                    text: "3"
                    onClicked: passwordField.text+= "3"
                }
                
                Button {
                    text: "4"
                    onClicked: passwordField.text+= "4"
                }
                
                Button {
                    text: "5"
                    onClicked: passwordField.text += "5"
                }
                
                Button {
                    text: "6"
                    onClicked: passwordField.text += "6"
                }
                
                Button {
                    text: "7"
                    onClicked: passwordField.text += "7"
                }
                
                Button {
                    text: "8"
                    onClicked: passwordField.text += "8"
                }
                
                Button {
                    text: "9"
                    onClicked: passwordField.text += "9"
                }
                
                Button {
                    text: ""
                    enabled: false
                }
                
                Button {
                    text: "0"
                    onClicked: passwordField.text += "0"
                }
                
                Button {
                    text: "Clear"
                    onClicked: passwordField.text = ""
                }
            }
            
            Button {
                text: "Log In"
                font.pixelSize: 16
                onClicked: {
                    // Добавьте код обработки нажатия кнопки
                }
            }
        }
    }
}
```
