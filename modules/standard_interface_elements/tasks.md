# Задания по теме «Элементы управления модуля QtQuick.Controls»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание 1

Разработать макет окна ввода логина и пароля. Установить защищенный режим отображения пароля (). Написать скрипты для проверки.

![](../../resources/99.png)

## Задание 2

Разработать макет окна ввода  цифрового пароля определенной длины. Пароль вводится нажатием на кнопки с цифрами. При каждом нажатии закрашивается в более интенсивный цвет очередная звездочка. При нажатии на кнопку Clear введенные символы сбрасываются, цвет звезд возвращается в исходное состояние
Написать скрипт проверки сформированного пароля для входа в систему. 

![](../../resources/100.png)

[Вариант кода](./answers.md)
