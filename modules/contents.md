#  Модули для курсов по&nbsp;дизайну мобильных приложений на&nbsp;Qt&nbsp;Quick

Copyright&nbsp;©&nbsp;2022‒2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Основы дизайна графического пользовательского интерфейса

*	[Исследование аудитории](./investigation)
    *	[Лекция](./investigation/lecture.fodp)
*	[Основные термины](./terms)
    *	[Руководство](./terms/manual.md)
    *	[Вопросы](./terms/tests.md)
    *	[Задания](./terms/tasks.md)
*	[Принципы дизайна интерфейсов мобильных приложений](./design_guidelines)
    *	[Руководство](./design_guidelines/manual.md)
    *	[Вопросы](./design_guidelines/tests.md)
    *	[Задания](./design_guidelines/tasks.md)
*	[Этапы разработки дизайн-проекта](./steps)
    *	[Руководство](./steps/manual.md)
    *	[Вопросы](./steps/tests.md)
    *	[Задания](./steps/tasks.md)

## Разработка прототипа

*	[Прототипирование в&nbsp;Figma](./prototyping_figma)
    *	[Руководство](./prototyping_figma/manual.md)
    *	[Вопросы](./prototyping_figma/tests.md)
    *	[Задания](./prototyping_figma/tasks.md)

## Разработка пользовательского интерфейса в&nbsp;QML

*	[Разработка приложения](./app_development)
    *	[Лекция](./app_development/lecture.fodp)
*	[Знакомство с&nbsp;QML](./qml_introduction)
    *	[Руководство](./qml_introduction/manual.md)
    *	[Вопросы](./qml_introduction/tests.md)
    *	[Задания](./qml_introduction/tasks.md)
    *	[Ответы на&nbsp;задания](./qml_introduction/answers.md)
*	[Позиционирование элементов интерфейса](./item_positioning)
    *	[Руководство](./item_positioning/manual.md)
    *	[Вопросы](./item_positioning/tests.md)
    *	[Задания](./item_positioning/tasks.md)
    *	[Ответы на&nbsp;задания](./item_positioning/answers.md)
*	[Изменение свойств объектов при взаимодействии с&nbsp;пользователем](./user_interaction)
    *	[Руководство](./user_interaction/manual.md)
    *	[Вопросы](./user_interaction/tests.md)
    *	[Задания](./user_interaction/tasks.md)
    *	[Ответы на&nbsp;задания](./user_interaction/answers.md)
*	[Работа со&nbsp;списками](./lists_models_representations)
    *	[Руководство](./lists_models_representations/manual.md)
    *	[Вопросы](./lists_models_representations/tests.md)
    *	[Задания](./lists_models_representations/tasks.md)
    *	[Ответы на&nbsp;задания](./lists_models_representations/answers.md)
*	[Состояния. Переходы. Анимации](./states_transitions_animations)
    *	[Руководство](./states_transitions_animations/manual.md)
    *	[Вопросы](./states_transitions_animations/tests.md)
    *	[Задания](./states_transitions_animations/tasks.md)
*	[Элементы управления модуля QtQuick.Controls](./standard_interface_elements)
    *	[Руководство](./standard_interface_elements/manual.md)
    *	[Вопросы](./standard_interface_elements/tests.md)
    *	[Задания](./standard_interface_elements/tasks.md)
    *	[Ответы на&nbsp;задания](./standard_interface_elements/answers.md)
*	[Переход по&nbsp;страницам приложения](./navigation)
    *	[Руководство](./navigation/manual.md)
    *	[Вопросы](./navigation/tests.md)
    *	[Задания](./navigation/tasks.md)
*	[Перспективы разработки](./prospects_for_development)
    *	[Руководство](./prospects_for_development/manual.md)
