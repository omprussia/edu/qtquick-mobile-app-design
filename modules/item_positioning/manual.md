# Позиционирование элементов интерфейса

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

Позиционирование элементов интерфейса является важным аспектом в дизайне. Оно  помогает пользователю быстро освоиться с интерфейсом и успешно выполнять свои задачи. Позиционирование элементов позволяет создать визуальную иерархию, выделяя важные элементы и определяя их отношения. Эффективное использование пространства экрана позволяет разместить все необходимые элементы, не перегружая интерфейс лишней информацией. В данной теме рассмотриваются различные способы позиционирования.

## Позиционирование через координаты

В QML можно позиционировать элементы по координатам, используя свойства `x`, `y`, `width` и `height`. Например, чтобы разместить кнопку в координатах x = 100, y = 200 с шириной 200 и высотой 50, можно использовать следующий код:

```qml
Button { x: 100 y: 200 width: 200 height: 50 }
```

По умолчанию системой визуальных координат, используемой в Qt Quick, являются координаты элементов. Это картезианская система координат с (0,0) в верхнем левом углу элемента. Ось x направлена вправо, ось y направлена вниз, так что нижний правый угол элемента находится в координатах (ширина, высота).

![](../../resources/66_1.png)

В приведенном примере прямоугольник с идентификатором rect1 имеет координаты (100, 100) относительно окна программы. Прямоугольник с идентификатором rect2 является его дочерним элементом, и его координаты отсчитываются от левого верхнего угла родителя.

```qml
Rectangle {
    id: rect1
    width: 200
    height: 200
    x:100
    y:100
    color:"lightblue"

    Rectangle {
        id: rect2
        width: 200
        height: 200
        x:100
     y:100
        color:"orange" 
    }  
}
```


Важно помнить, что объекты позиционируются в системе координат своего родительского элемента.

## Позиционирование через привязанные свойства

Позиционирование через привязанные свойства в QML означает, что позиция элемента на экране определяется не абсолютными координатами, а относительно других элементов или края экрана. 

Пример кода QML, где используется позиционирование через привязанные свойства:

```qml
import QtQuick 2.0

Rectangle {
    width: 100
    height: 100

    property int offset: 10

    // Используем привязанное свойство для позиционирования
    x: parent.width - width - offset
    y: parent.height - height - offset
}
```


В данном примере используется элемент `Rectangle`, который имеет ширину и высоту 100 и свойство offset со значением 10. Свойства x и y используют привязанные свойства, чтобы позиционировать элемент в правом нижнем углу родительского элемента, используя свойство offset для отступа.

## Позиционирование через якоря (anchors)

Крайне удобным способом позиционирования различных QML компонентов является использование якорей (anchors).

У каждого элемента есть набор вложенных свойств `anchors`, благодаря которым можно полностью управлять его расположением относительно других элементов.

![](../../resources/67.png)
Перечень данных свойств:
*	`top` — задает привязку верхней границы элемента;
*	`bottom` — задает привязку нижней границы элемента;
*	`left` — задает привязку левого края элемента;
*	`right` — задает привязку правого края элемента;
*	`horizontalCenter` — задает привязку линии горизонтального центра;
*	`verticalCenter` — задает привязку линии вертикального центра;
*	`baseline` — (для текста) задает привязку базовой линии текста;
*	`fill` — задает заполнение элементом указанной области;
*	`centerIn` — задает привязку к центру указанной области.

С помощью этих свойств можно позиционировать выбранный элемент относительно его родительского элемента или элементов того же уровня, что и он сам.

Пример позиционирования во весь экран
Привязка к центру
Привязка к краям

![](../../resources/68_1.png)

```qml 
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
 width: 640
 height: 480
 visible: true
 title: qsTr("QML Anchors")
 color: "light grey"

      Rectangle {
          anchors.top: parent.top
          anchors.left: parent.left
          anchors.bottom: parent.bottom
          width: parent.width/3
          color: "coral"
       }
      Rectangle {
          anchors.top: parent.top
          anchors.right: parent.right
          anchors.bottom: parent.bottom
          width: parent.width/3
          color: "deep sky blue"
       }
   }
```

В этом примере используются два прямоугольника, которые позиционируются относительно родительского прямоугольника с помощью якорей. Первый прямоугольник привязан к верхней и левой стороне родительского прямоугольника, а второй прямоугольник привязан к верхней и правой стороне родительского прямоугольника. Ширина каждого прямоугольника составляет треть от родительского окна.

Заполнять все доступное пространство указанного по id объекта можно с помощью свойства __anchors.fill.__

![](../../resources/69_1.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
  width: 640
  height: 480
  visible: true
  title: qsTr("QML Anchors")
  color: "grey"
  Rectangle {
      color: "deep sky blue"
      anchors.fill: parent
    }
}
```

Применение свойства `anchors.fill` аналогично применению набора свойств:

```qml
anchors.left: parent.left
anchors.right: parent.right
anchors.top: parent.top
anchors.bottom: parent.bottom
```

В данном примере размер прямоугольника напрямую зависит от размера окна, к границам которой он привязан.

Если размеры элемента заданы, но нужно привязать его к центру родительского объекта, на помощь приходит свойство __anchors.centerIn.__

![](../../resources/70_1.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
 width: 640
 height: 480
 visible: true
 title: qsTr("QML Anchors")
 color: "light grey"
 Rectangle {
     width:parent.width/3
     height:parent.height/3
     color: "deep sky blue"
     anchors.centerIn: parent
    }
}
```

В приведенном примере высота и ширина прямоугольника составляют треть от размеров родительского окна.

## Отступы при позиционировании 

Когда речь заходит о позиционировании, встает необходимость добавлять отступы между элементами.  Для этого можно использовать свойство `margins`. `Margins` задает размер пустого пространства между линиями `top`, `bottom`, `left`, `right` и самим элементом в пикселах. Реализуются отступы свойствами:

*	`anchors.topMargin` — отступ от линии верхнего якоря;
*	`anchors.bottomMargin` — отступ от линии нижнего якоря;
*	`anchors.leftMargin` — отступ от линии левого якоря;
*	`anchors.rightMargin` — отступ от линии правого якоря.

Есть обобщающее свойство anchors.margins, задающее все четыре предыдущих одновременно.

![](../../resources/71.png)

Важно помнить, что якоря используются с родительскими элементами или элементами того же уровня, что и выравниваемый объект.
Определяя позиции элементов, определите роли элементов интерфейса:

*	фиксированные элементы (всегда находятся в определенном месте);
*	доминантные элементы (будут являться опорой для всех остальных элементов);
*	элементы, реагирующие на изменение размеров доминантных элементов (привязаны к ним якорями).

В примере, приведенном ниже, фиксированы прямоугольники вдоль верхнего и нижнего краев экрана. Их высота не меняется. Прямоугольник между ними может менять свою высоту при изменении размеров окна. Его вертикальные стороны привязаны к родительским вертикальным сторонам, а горизонтальные — к горизонтальным сторонам прямоугольников того же уровня, что и он сам.

![](../../resources/72_1.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("QML Anchors")
    color: "light grey"
    Rectangle {
        id:rect_top
        color: "coral"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right:parent.right
        anchors.margins:10
        height:100
    }
    Rectangle {
        id:rect_mid
        color: "deep sky blue"
        anchors.top: rect_top.bottom
        anchors.left: parent.left
        anchors.right:parent.right
        anchors.bottom: rect_bot.top
        anchors.margins:10
    }
    Rectangle {
        id:rect_bot
        color: "green"
        
        anchors.left: parent.left
        anchors.right:parent.right
        anchors.bottom: parent.bottom
        height:100
        anchors.margins:10
    }
}
```

Следует помнить, что отступы работают только когда определены якоря, т.к. они вычисляются относительно привязки.

## Порядок отрисовки элементов

Элементы интерфейса отрисовываются по иерархии в порядке следования. Сначала отрисовывается корневой элемент, потом первый потомок с вложенными объектами, затем второй и т.д.

Порядок может быть изменен применением `z-координаты`. По умолчанию значение `z` равно нулю, но оно может принимать положительные и отрицательные значения. Чем меньше `z`, тем раньше произойдет отрисовка элемента. Если у двух объектов значение `z` одинаково, то порядок отрисовки будет определяться иерархией.

![](../../resources/73.png)

Код последнего примера:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("")
    Item{
        Rectangle{
            id:rect_red
            x:0;y:0
            width:200;height:200
            color:"red"
            z:2
            Text{
                text:"My z = "+rect_red.z
                anchors.bottom:parent.bottom
                font.pixelSize: 24
            }
        }
        Rectangle{
            id:rect_green
            x:100;y:100
            width:200;height:200
            color:"green"
            z:1
            Text{
                text:"My z = "+rect_green.z
                anchors.bottom:parent.bottom
                font.pixelSize: 24
            }
        }
        Rectangle{
            id:rect_blue
            x:200;y:200
            width:200;height:200
            color:"blue"
            Text{
                text:"My z = "+rect_blue.z
                anchors.bottom:parent.bottom
                font.pixelSize: 24
            }
        }
    }
}
```

## Порядок отрисовки объектов с иерархиями

Координата `z` в первую очередь влияет на отрисовку элементов одного уровня. В первом приведенном примере на одном уровне красный и синий прямоугольники. `Z-координата` синего не задана, значит, имеет значение 0. Сначала отрисовывается красный и его дочерний зеленый, затем синий.

Во втором примере `z-координата` синего нулевая. Это меньше, чем z=1 красного, поэтому сначала отрисован синий. Красный и его дочерний зеленый прямоугольники отрисовываются после синего.  

![](../../resources/74.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("")
    Item{
        Rectangle{
            id:rect_red
            x:100;y:0
            width:200;height:200
            color:"red"
            z:1
            Text{
                text:"z = "+rect_red.z
                anchors.bottom:parent.bottom
                font.pixelSize: 24
            }
            
            
            Rectangle{
                id:rect_green
                x:150; y:150
                width:200;height:200
                color:"green"
                Text{
                    text:"z = "+rect_green.z
                    anchors.bottom:parent.bottom
                    font.pixelSize: 24
                }
            }
            
        }
        Rectangle{
            id:rect_blue
            x:150; y:120
            width:200;height:200
            color:"blue"
            Text{
                text:"z = "+rect_blue.z
                anchors.bottom:parent.bottom
                font.pixelSize: 24
            }
        }
    }
}
```

## Компонент (повторно используемый пользовательский элемент)

Для  того, чтобы некоторый элемент интерфейса можно было использовать повторно и не загромождать код основного окна повторяющимися строками разметки, такой элемент принято описывать в отдельном qml-файле проекта. Название файла должно начинаться с заглавной буквы. По названию файла этот компонент можно вызывать без строки импорта в соседнем qml-файле проекта и с импортированием из каталога в любом другом qml-файле проекта.

В приведенном примере три раза вызывается один и тот же компонент, состоящий из прямоугольника шириной 300 на 100 случайного цвета (компоненты rgb заданы рандомно с помощью  математической функции random()). Прозрачность цвета равна 0.8. Код для прямоугольника находится в файле Color_rect.qml. В файле main.qml меняются лишь координаты вывода.

![](../../resources/75.png)

Файл Color_rect.qml.

```qml
import QtQuick 2.0

Item {
    Rectangle{
        width:300
        height: 100
        color: Qt.rgba(Math.random(), Math.random(),Math.random(),0.8)
    }
}
```

Файл main.qml.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Component")
    
    Color_rect{
        x:50
        y:50
    }
    Color_rect{
        x:150
        y:150
    }
    Color_rect{
        x:250
        y:250
    }
}
```

При вызове пользовательского элемента в главном окне при необходимости можно переопределить ряд его свойств. Для этого в пользовательском элементе объявляется свойство (property) как alias (псевдоним). Это позволяет ссылаться на свойство или значение с использованием псевдонима вместо фактического имени свойства. Псевдонимы полезны для создания повторно используемых компонентов и для упрощения изменения реализации компонента без затрагивания остальной части приложения.

```qml
import QtQuick 2.0

Item {
    property alias comColor: rect1.color
    
    Rectangle{
        id:rect1
        
        width:200
        height: 100
        border.width: 1
    }
}
```

В главном файле определено значение цвета для элемента, описанного в файле Comp.qml, по alias-свойству comColor.

```qml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Alias")
    
    Comp{
        comColor:"red"
    }
}
```

## Компоновщики блоков. Layouts

Рассмотрим возможность позиционирования блоков информации с помощью компоновщиков (`layouts`). Все типы компоновщиков содержатся в модуле Quit Quick Layouts. Для его использования сначала необходимо импортировать доступную версию этого модуля с помощью команды import. Это сделает доступным такие виды компоновщиков, как `GrigLayout` (табличный компоновщик), `RowLayout`` (компоновщик строки) и ColumnLayout` (компоновщик столбца). Использовать компоновщики можно как по отдельности, так и вложенными друг в друга, что позволяет очень гибко реализовывать необходимые варианты размещения информации в плоскости экрана.

Для позиционирования объектов в компоновщике используем функционал вложенных свойств `Layout`.:

*	`Layout.minimumWidth` — минимальная ширина объекта;
*	`Layout.minimumHeight` — минимальная высота объекта;
*	`Layout.preferredWidth` — предпочтительная ширина объекте;
*	`Layout.preferredHeight` — предпочтительная высота объекта;
*	`Layout.maximumWidth` — максимальная ширина объекта;
*	`Layout.maximumHeight` — максимальная высота объекта;
*	`Layout.fillWidth` — заполнение по ширине;
*	`Layout.fillHeight` — заполнение по высоте;
*	`Layout.alignment` — выравнивание в слое;
*	`Layout.margins` — установка величины всех отступов;
*	`Layout.leftMargin` —  левый отступ
*	`Layout.rightMargin` —  правый отступ;
*	`Layout.topMargin` — отступ от верхнего края;
*	`Layout.bottomMargin` —  отступ от нижнего края.

### Компоновщик ColumnLayout

Этот тип компоновщика выстраивает объекты в [столбец](https://doc.qt.io/qt-5.6/qml-qtquick-layouts-columnlayout.html).

Для примера создан пользовательский элемент типа прямоугольник случайного цвета, содержащий внутри себя текст со значением цвета в шестнадцатеричной системе.

```qml
import QtQuick 2.0

Rectangle{
    id:rect1
    
    width:300
    height: 100
    border.width: 1
    color:Qt.rgba(Math.random(),Math.random(),Math.random(),0.7)
    Text{
        text:"My color: "+rect1.color
        anchors.centerIn: rect1
        font.pixelSize: 24
    }
}
```

Этот элемент вставлен в ячейки компоновщика ColumnLayout.

![](../../resources/76_1.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("ColumnLayout")
    ColumnLayout{
        id:cl
        spacing:10
        
        Comp{   }
        Comp{   }
        Comp{   }
    }
}
```

Внесем некоторые изменения в рассмотренный пример. Применим свойства выравнивания содержимого `Layout.aligment`. Поскольку ширина компоновщика определяется содержимым (а ширина прямоугольников одинакова), то мы не увидим эффект, пока не растянем содержимое на все окно родителя (anchors.fill:parent).
Для того, чтобы окно приложения при уменьшении не обрезало содержимое, устанавливаем минимальную ширину окна равной ширине содержимого компоновщика с учетом отступов: `minimumWidth: cl.implicitWidth +2*myMargin`.      

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("ColumnLayout")
    
    property int myMargin:10 // Устанавливаем отступы
    minimumWidth:cl.implicitWidth+2*myMargin // Минимальная ширина окна
    ColumnLayout{
        id:cl
        spacing: myMargin
        anchors.fill:parent // Иначе не увидим выравнивание
        
        Comp{
            Layout.alignment: Qt.AlignLeft}
        
        Comp{
            Layout.alignment: Qt.AlignHCenter}
        Comp{
            Layout.alignment: Qt.AlignRight }
        Comp{
            Layout.fillWidth: true
            Layout.minimumWidth:400
        }
        Item{
            Layout.fillHeight: true   } // Подпираем снизу содержимое
    }
}
```

![](../../resources/77.png)

### Компоновщик RowLayout

Данный тип компоновщика отличается от описанного выше `ColumnLayout` только ориентацией выстраивания содержимого в [строку](https://doc.qt.io/qt-5.6/qml-qtquick-layouts-rowlayout.html).

![](../../resources/78_1.png)

### Компоновщик `GridLayout`

Данный тип компоновщика упорядочивает содержимое в [сетке таблицы](https://doc.qt.io/qt-5.6/qml-qtquick-layouts-gridlayout.html). Для него необходимо указать количество строк и столбцов.
Поскольку для расположения некоторых элементов может понадобиться объединение ячеек, можно использовать функционал свойств:

*	`Layout.rowSpan` — указывает, на сколько строк должен быть растянут объект;
*	`Layout.columnSpan` — указывает, на сколько колонок должен быть растянут объект.

Свойства `GridLayout`:

*	`columnSpacing : real` — пробелы между колонками (разрыв между объектами);
*	`columns : int` — количество колонок;
*	`flow : enumeration`` — направление расположения объектов в GridLayout`;
*	`GridLayout.LeftToRight` (default) — слева направо;
*	`GridLayout.TopToBottom` — сверху вниз;
*	`layoutDirection : enumeration` — направление перечисления объектов при заданном flow;
*	`Qt.LeftToRight` (default) — слева направо;
*	`Qt.RightToLeft` — справа налево;
*	`rowSpacing : real` — пробелы между строками (разрыв между объектами);
*	`rows : int` — количество строк.

В результате первый элемент строки будет в самом верху, а последний элемент строки будет в самом низу. При наличии двух строк элементы первой строки будут располагаться справа.

![](../../resources/79_1.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("GridLayout")
    GridLayout{
        id:cl
        rows:2; columns:3
        Comp{   }
        Comp{   }
        Comp{   }
        Comp{   }
        Comp{   }
        Comp{   }
    }
}
```

Пример использования GridLayout с объединенными ячейками:

![](../../resources/80.png)

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12

Window {
    width: 640
    height: 480
    visible: true
    GridLayout {
        id: grid
        anchors.fill: parent
        rows: 4
        columns: 4
        
        Rectangle {
            color: "red"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.row: 1
            Layout.column: 2
        }
        Rectangle {
            color: "yellow"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.row: 3
            Layout.column: 1
        }
        
        Rectangle {
            color: "blue"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 1
            Layout.rowSpan: 2
            Layout.row: 1
            Layout.column: 1
        }
        
        Rectangle {
            color: "lightgrey"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 1
            Layout.rowSpan: 1
            Layout.row: 2
            Layout.column: 2
        }
        
        Rectangle {
            color: "green"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 1
            Layout.rowSpan: 2
            Layout.row:2
            Layout.column: 3
        }
        
    }
}
```

![](../../resources/81.png)
