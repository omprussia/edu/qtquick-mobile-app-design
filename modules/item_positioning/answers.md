# Возможные ответы на задания по теме «Позиционирование элементов интерфейса»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Lego

![](../../resources/107.png)

Main.qml

```qml
Rectangle {
    id:rect1
    color: "white"
    border.color: "grey"
    width: parent.width
    height: parent.height
    Text {
        text: "Task 1"
        anchors.top:parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins:5
    }
    Rectangle{
        id:rect_red
        anchors.centerIn:  rect1
        width: 200
        height:200
        color: "red"
    }
    anchors.margins: 5
    MyComponent{
        anchors.bottom: rect_red.top
        anchors.bottomMargin:3
        anchors.horizontalCenter:rect_red.left
    }
    MyComponent{
        anchors.top: rect_red.bottom
        anchors.horizontalCenter:rect_red.right
        anchors.topMargin:3
    }
}
```

MyComponent.gml

```qml
Rectangle{
    width:100
    height: 100
    color: Qt.rgba(Math.random(), Math.random(),Math.random(),0.8)
}
```

## Layout

![](../../resources/108.png)

Main.qml:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3

Window {

   width: 360
   height: 640
   visible: true
   title: qsTr("Task_for_Layout")
   Rectangle{
       id:fon
       anchors.fill:parent
       color: "white smoke"

       Rectangle{
            id:header
            height:60
            anchors.left:fon.left
            anchors.right:fon.right
            anchors.top:fon.top
            color: "gainsboro"
            Text{text:"Header"; anchors.centerIn: parent}
        }
        Rectangle{
            id:cont
            anchors.top:header.bottom
            anchors.bottom:footer.top
            anchors.left: fon.left
            anchors.right: fon.right
            anchors.margins: 10
            color: "white smoke"
            border.color:"silver"
            Text{text:"Content"; anchors.centerIn: parent}
        }

        Rectangle{
            id:footer
            height:62
            anchors.left:fon.left
            anchors.right:fon.right
            anchors.bottom:fon.bottom
            color: "gainsboro"

            RowLayout{
                anchors.left:footer.left
                anchors.right:footer.right

                MyComponent{
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{text:"1"; anchors.centerIn: parent}
                }
                MyComponent{
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{text:"2"; anchors.centerIn: parent}
                }
                MyComponent{
                    Layout.fillWidth:true
                    Layout.fillHeight: true
                    Text{text:"3"; anchors.centerIn: parent}
                }
            }
        }
   }
}
```

MyComponent.qml:

```qml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3

Rectangle{
    width:100
    height:60
    color:"light gray"
    border.color: "silver"
    border.width: 1
}
```
